#
# SPDX-License-Identifier: MIT
# Copyright (c) 2024 Tano Systems LLC. All rights reserved.
#

# Override original do_copy_target_rustlibs()
# (replace hardcoded /usr/lib in recipe sysroot by ${libdir})
do_copy_target_rustlibs() {
    # Chromium needs a single Rust sysroot that contains the rustlibs for both
    # the host and target, so we copy the target rustlibs to the native sysroot.
    rustlib_src_dir="${RECIPE_SYSROOT}${libdir}/rustlib/${TARGET_ARCH}"*
    cp -r $rustlib_src_dir "${RECIPE_SYSROOT_NATIVE}/usr/lib/rustlib"
}
