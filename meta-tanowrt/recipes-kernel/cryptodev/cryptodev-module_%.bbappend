#
# SPDX-License-Identifier: MIT
# Copyright (c) 2020, 2023 Tano Systems LLC. All rights reserved.
#
PR:append = ".tano1"

inherit tanowrt-kernel-module-split

KERNEL_MODULE_AUTOLOAD += "cryptodev"
