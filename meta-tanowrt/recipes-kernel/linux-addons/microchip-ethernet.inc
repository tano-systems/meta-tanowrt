#
# SPDX-License-Identifier: MIT
#
# Microchip DSA switch drivers
#
# This file Copyright (C) 2021-2024 Tano Systems LLC. All rights reserved.
# Authors: Anton Kikin <a.kikin@tano-systems.com>
#

PV:append = "+ksz4"
FILESEXTRAPATHS:prepend := "${THISDIR}/microchip-ethernet/kernel-${LINUX_VERSION_SHORT}:${THISDIR}/microchip-ethernet:"

MICROCHIP_ETHERNET_ENABLE ?= "1"
MICROCHIP_ETHERNET_CONFIGS ?= "ksz9477-spi"

MICROCHIP_ETHERNET_GIT_URI      ?= "git://${TANO_SYSTEMS_MISC_GIT_BASE_URL}/microchip-ethernet.git"
MICROCHIP_ETHERNET_GIT_BRANCH   ?= "master"
MICROCHIP_ETHERNET_GIT_PROTOCOL ?= "${TANO_SYSTEMS_MISC_GIT_PROTOCOL}"
MICROCHIP_ETHERNET_GIT_SRCREV   ?= "b2cc796cf6e473da0c1edc814a0f3fdc1270a42c"

MICROCHIP_ETHERNET_DESTDIR ?= "microchip-ethernet"
MICROCHIP_ETHERNET_SRCDIR ?= "${WORKDIR}/${MICROCHIP_ETHERNET_DESTDIR}"

MICROCHIP_ETHERNET_FILES = "\
	${MICROCHIP_ETHERNET_GIT_URI};branch=${MICROCHIP_ETHERNET_GIT_BRANCH};protocol=${MICROCHIP_ETHERNET_GIT_PROTOCOL};name=microchip-ethernet;destsuffix=${MICROCHIP_ETHERNET_DESTDIR} \
"

SRCREV_microchip-ethernet = "${MICROCHIP_ETHERNET_GIT_SRCREV}"

def get_data_for_tag(metadata, tag):
    for key, value in metadata.items():
        for k in key.split(','):
            if tag == k.strip():
                return value

    return None

def apply_metadata(metadata_json, tags, src_dir, dst_dir, d):
    import os
    import json
    import shutil

    if not os.path.exists(metadata_json):
        bb.fatal('Metadata JSON at "%s" is not exists' % metadata_json)

    bb.note('Applying metadata from "%s"...' % metadata_json);
    bb.note('Source directory: %s' % src_dir);
    bb.note('Destination directory: %s' % dst_dir);

    with open(metadata_json) as j:
        metadata = json.load(j)

    if 'version' not in metadata or metadata['version'] != '1':
        bb.fatal('Unsupported metadata JSON version')

    if 'data' not in metadata:
        bb.fatal('Invalid metadata JSON format')

    patches = []
    files = []

    for tag in tags:
        bb.note('Reading data for tag "%s"...' % tag)

        data = get_data_for_tag(metadata['data'], tag)
        if not data:
            bb.fatal('Can\'t find data for tag "%s"' % tag)

        if 'patches' in data:
            bb.note('Using patches from tag "%s"...' % tag)
            for patch in data['patches']:
                patch = patch.replace('%tag%', tag)
                if os.path.exists(os.path.join(src_dir, patch)):
                    bb.note('Patch: %s' % patch)
                    patches.append(patch)
                else:
                    bb.warn('Patch "%s" is not exists at "%s", ignoring' % (patch, src_dir))

        if 'files' in data:
            bb.note('Using files from tag "%s"...' % tag)
            for file in data['files']:
                if 'src' not in file and 'dst' not in file:
                    bb.fatal('Invalid metadata JSON format')

                file['src'] = file['src'].replace('%tag%', tag)
                file['dst'] = file['dst'].replace('%tag%', tag)

                if os.path.exists(os.path.join(src_dir, file['src'])):
                    bb.note('File: %s -> %s' % (file['src'], file['dst']))
                    files.append(file)
                else:
                    bb.warn('File "%s" is not exists at "%s", ignoring' % (file['src'], src_dir))

    workdir = d.getVar('WORKDIR')
    srcsubdir = d.getVar('S')

    for patch in patches:
        patchpath = os.path.join(src_dir, patch)

        shellcmd = [ "git", "--work-tree=%s" % dst_dir, "am", patchpath ]
        try:
            oe.patch.runcmd(["sh", "-c", " ".join(shellcmd)], dst_dir)
            bb.note('Applied patch "%s"' % patchpath)
        except oe.patch.CmdError as err:
            bb.fatal('Failed to apply patch "%s":\n%s' % (patchpath, err.output))

    for file in files:
        src = os.path.join(src_dir, file['src'])
        dst = os.path.join(dst_dir, file['dst'])

        try:
            os.mkdir(os.path.dirname(dst))
            bb.note('Created directory "%s"' % os.path.dirname(dst))
        except OSError:
            pass

        shutil.copy(src, dst)
        bb.note('Copyied file from "%s" to "%s"' % (src, dst))

do_microchip_ethernet_copy_src[deptask] += "do_patch do_metadata"
python do_microchip_ethernet_copy_src() {
    src_dir = d.getVar('MICROCHIP_ETHERNET_SRCDIR')
    dst_dir = d.getVar('S')
    kernel_version_short = d.getVar('LINUX_VERSION_SHORT')

    apply_metadata(os.path.join(src_dir, "metadata.json"), [ 'common', kernel_version_short ], src_dir, dst_dir, d)
}

python __anonymous() {
    enable = d.getVar('MICROCHIP_ETHERNET_ENABLE', True) or "0"
    if enable != "1":
        return

    configs = (d.getVar('MICROCHIP_ETHERNET_CONFIGS', True) or "").split()
    files   = (d.getVar('MICROCHIP_ETHERNET_FILES', True) or "").split()

    for config in configs:
        d.appendVar('SRC_URI', ' file://%s.cfg ' % (config))

    d.appendVar('SRC_URI', ' '.join(files))
    bb.build.addtask("do_microchip_ethernet_copy_src", "do_kernel_configme", "do_patch", d)

    d.appendVarFlag('do_microchip_ethernet_copy_src', 'vardeps', ' MICROCHIP_ETHERNET_GIT_SRCREV MICROCHIP_ETHERNET_FILES')
    d.appendVarFlag('do_fetch', 'vardeps', ' MICROCHIP_ETHERNET_GIT_SRCREV MICROCHIP_ETHERNET_FILES')
    d.appendVarFlag('do_patch', 'vardeps', ' MICROCHIP_ETHERNET_GIT_SRCREV')
}
