#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2024 Tano Systems LLC. All rights reserved.
# Authors: Anton Kikin <a.kikin@tano-systems.com>
#
# Yocto Linux kernel 6.1 (standard)
#

# Skip processing of this recipe if it is not explicitly specified as the
# PREFERRED_PROVIDER for virtual/kernel.
python () {
    if d.getVar("KERNEL_PACKAGE_NAME") == "kernel" and d.getVar("PREFERRED_PROVIDER_virtual/kernel") != "linux-tano-yocto":
        raise bb.parse.SkipRecipe("Set PREFERRED_PROVIDER_virtual/kernel to linux-tano-yocto to enable it")
}

KERNEL_SRC_URI ?= "git://git.yoctoproject.org/linux-yocto.git"

KERNEL_SRC_BRANCH:qemuarm ?= "v6.1/standard/arm-versatile-926ejs"
KERNEL_SRC_BRANCH:qemuarm64 ?= "v6.1/standard/qemuarm64"
KERNEL_SRC_BRANCH:qemux86 ?= "v6.1/standard/base"
KERNEL_SRC_BRANCH:qemux86-64 ?= "v6.1/standard/base"
KERNEL_SRC_BRANCH ?= "v6.1/standard/base"

KERNEL_SRC_SRCREV_machine:qemuarm ?= "2f7e672f9677d3cc448ec7e004763f76f95c7fe0"
KERNEL_SRC_SRCREV_machine:qemuarm64 ?= "d025fe8c17718aa4c837bfafee0f3aa0f830bc75"
KERNEL_SRC_SRCREV_machine:qemux86 ?= "d025fe8c17718aa4c837bfafee0f3aa0f830bc75"
KERNEL_SRC_SRCREV_machine:qemux86-64 ?= "d025fe8c17718aa4c837bfafee0f3aa0f830bc75"
KERNEL_SRC_SRCREV ?= "d025fe8c17718aa4c837bfafee0f3aa0f830bc75"

LINUX_VERSION ?= "6.1.78"
LINUX_KERNEL_TYPE ?= "standard"
PV = "${LINUX_VERSION}+git${SRCPV}"

# Append to the MACHINE_KERNEL_PR so that a new SRCREV will cause a rebuild
MACHINE_KERNEL_PR:append = "tano0"
PR = "${MACHINE_KERNEL_PR}"

require recipes-kernel/linux/linux-tano.inc
require recipes-kernel/linux/linux-tano-yocto.inc

# Look in the generic major.minor directory for files
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}-6.1:"

YOCTO_KERNEL_CACHE_BRANCH = "yocto-6.1"
YOCTO_KERNEL_CACHE_SRCREV = "ea5365f818fb6031ec97b8ae7a88bb83001b901e"

SECTION = "kernel"
DESCRIPTION = "Yocto Linux kernel"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"
