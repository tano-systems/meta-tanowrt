#
# SPDX-License-Identifier: MIT
#
# Linux device driver for RealTek RTL8125 2.5 Gigabit Ethernet
# controllers with PCI-Express interface.
#
# https://github.com/friendlyarm/r8125
#
# Copyright (C) 2024 Anton Kikin <a.kikin@tano-systems.com>
#

PR = "tano0"
PV = "9.010.01+git${SRCPV}"

DESCRIPTION = "Linux device driver for RealTek RTL8125 2.5 Gigabit Ethernet controllers"
HOMEPAGE = "https://github.com/friendlyarm/r8125"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://r8125.h;endline=28;md5=8958f9b617f25fb4f15a4fbd2775c258"
SECTION = "net"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/patches:${THISDIR}/${PN}/files:"

SRC_URI = "git://github.com/friendlyarm/r8125.git;protocol=https;branch=main"
S = "${WORKDIR}/git"

# 23.04.2024
SRCREV = "c4cb053bff438fac317e2371cce45985098e4c49"

do_configure[depends] += "virtual/kernel:do_shared_workdir"

inherit tanowrt-kernel-module

MODULES_INSTALL_TARGET = "install"

EXTRA_OEMAKE += "\
	KSRC=${STAGING_KERNEL_BUILDDIR} \
	RTKDIR=kernel/drivers/net \
"

RPROVIDES:${PN} += "kernel-module-r8125"
