#
# SPDX-License-Identifier: MIT
# Copyright (C) 2024 Anton Kikin <a.kikin@tano-systems.com>
#

PR = "tano0"
SUMMARY = "Extra console utilities"
DESCRIPTION = "The set of extra console utilities packages"
LICENSE = "MIT"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS:${PN} = "\
	ncurses \
	ncurses-terminfo \
	htop \
	tmux \
	jq \
	iotop \
	vim-xxd \
"
