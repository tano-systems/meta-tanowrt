#
# SPDX-License-Identifier: MIT
#
# modpoll is a command line based Modbus master simulator and test utility.
# modpoll is using the FieldTalk Modbus driver.
#
# This file Copyright (C) 2019-2020,2024 Anton Kikin <a.kikin@tano-systems.com>
#
PR = "tano0"

SUMMARY = "FieldTalk Modbus Master Simulator"
HOMEPAGE = "https://www.modbusdriver.com/modpoll.html"
LICENSE = "proconX-LICENSE-FREE"
LIC_FILES_CHKSUM = "file://LICENSE-FREE.txt;md5=60cf88fcfd1f3ebe7d3780ba8bbc53b6"

COMPATIBLE_HOST = "(i.86.*|x86_64.*|arm.*|aarch64.*)-linux.*"

SRC_URI = "https://www.modbusdriver.com/downloads/modpoll.tgz"
SRC_URI[md5sum] = "7cd08140507ca7ba6ce1dd00c02b1f31"
SRC_URI[sha256sum] = "4552d0a373284b2d88da8dab6229276190742c680dfca793d949ef4c91f52d1d"

S = "${WORKDIR}/modpoll"

INSANE_SKIP:${PN}:append = "already-stripped"

ARCH_DIR:x86-64 = "x86_64-linux-gnu"
ARCH_DIR:i586 = "i686-linux-gnu"
ARCH_DIR:i686 = "i686-linux-gnu"
ARCH_DIR:arm = "arm-linux-gnueabihf"
ARCH_DIR:aarch64 = "aarch64-linux-gnu"

do_install () {
	install -m 0755 -d ${D}${bindir}
	install -m 0755 ${S}/${ARCH_DIR}/modpoll ${D}${bindir}/modpoll
}
