#
# SPDX-License-Identifier: MIT
# Copyright (c) 2023 Tano Systems LLC. All rights reserved.
#

TANO_SYSTEMS_HOMEPAGE = "https://tano-systems.com"

TANOWRT_HOMEPAGE  ?= "https://gitlab.com/tano-systems/meta-tanowrt"
TANOWRT_ISSUES    ?= "https://gitlab.com/tano-systems/meta-tanowrt/-/issues"
TANOWRT_GIT_WEBUI ?= "https://gitlab.com/tano-systems/meta-tanowrt"

TANO_SYSTEMS_GIT_BASE_URL ?= "gitlab.com/tano-systems"
TANO_SYSTEMS_GIT_PROTOCOL ?= "https"

TANO_SYSTEMS_LUCI_GIT_BASE_URL ?= "${TANO_SYSTEMS_GIT_BASE_URL}/luci"
TANO_SYSTEMS_LUCI_GIT_PROTOCOL ?= "${TANO_SYSTEMS_GIT_PROTOCOL}"

TANO_SYSTEMS_TOOLS_GIT_BASE_URL ?= "${TANO_SYSTEMS_GIT_BASE_URL}/tools"
TANO_SYSTEMS_TOOLS_GIT_PROTOCOL ?= "${TANO_SYSTEMS_GIT_PROTOCOL}"

TANO_SYSTEMS_MISC_GIT_BASE_URL ?= "${TANO_SYSTEMS_GIT_BASE_URL}/misc"
TANO_SYSTEMS_MISC_GIT_PROTOCOL ?= "${TANO_SYSTEMS_GIT_PROTOCOL}"
