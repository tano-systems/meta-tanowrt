[ -e /tmp/.failsafe ] && export FAILSAFE=1

if [ -f "/tmp/banner" ]; then
	cat "/tmp/banner"
elif [ -f "/etc/banner" ]; then
	cat "/etc/banner"
fi

[ -n "$FAILSAFE" ] && cat /etc/banner.failsafe

grep -Fsq '/ overlay ro,' /proc/mounts && {
	echo 'Your overlay partition seems full and overlayfs is mounted read-only.'
	echo 'Please try to remove files from /overlay/upper/... and reboot!'
}

export PATH="/usr/sbin:/sbin:/usr/bin:/bin"
export HOME=$(grep -e "^${USER:-root}:" /etc/passwd | cut -d ":" -f 6)
export HOME=${HOME:-/root}
export PS1='\[\e[1;32m\][\u@\h \w]\$\[\e[0m\] '
export ENV=/etc/shinit

case "$TERM" in
	xterm*|rxvt*)
		export PS1='\[\e]0;\u@\h: \w\a\]'$PS1
		;;
esac

[ -n "$FAILSAFE" ] || {
	for FILE in /etc/profile.d/*.sh; do
		[ -e "$FILE" ] && . "$FILE"
	done
	unset FILE
}

if [ -t 0 -a $# -eq 0 ]; then
	if [ ! -x /usr/bin/resize ] ; then
		if [ -n "$BASH_VERSION" ] ; then
			# Optimized resize funciton for bash
			resize() {
				local x y
				IFS='[;' read -t 2 -p $(printf '\e7\e[r\e[999;999H\e[6n\e8') -sd R _ y x _
				[ -n "$y" ] && \
				echo -e "COLUMNS=$x;\nLINES=$y;\nexport COLUMNS LINES;" && \
				stty cols $x rows $y
			}
		else
			# Portable resize function for ash/bash/dash/ksh
			# with subshell to avoid local variables
			resize() {
				(o=$(stty -g)
				stty -echo raw min 0 time 2
				printf '\0337\033[r\033[999;999H\033[6n\0338'
				if echo R | read -d R x 2> /dev/null; then
					IFS='[;R' read -t 2 -d R -r z y x _
				else
					IFS='[;R' read -r _ y x _
				fi
				stty "$o"
				[ -z "$y" ] && y=${z##*[}&&x=${y##*;}&&y=${y%%;*}
				[ -n "$y" ] && \
				echo "COLUMNS=$x;"&&echo "LINES=$y;"&&echo "export COLUMNS LINES;"&& \
				stty cols $x rows $y)
			}
		fi
	fi
	# Use the EDITOR not being set as a trigger to call resize
	# and only do this for /dev/tty[A-z] which are typically
	# serial ports
	if [ -z "$EDITOR" -a "$SHLVL" = 1 ] ; then
		case $(tty 2>/dev/null) in
			/dev/tty[A-z]*) resize >/dev/null;;
		esac
	fi
fi

export EDITOR="nano"

if ( grep -qs '^root::' /etc/shadow && \
     [ -z "$FAILSAFE" ] )
then
cat << EOF

=== WARNING! ========================================================

    There is no root password defined on this device!
    Use the "passwd" command to set up a new password
    in order to prevent unauthorized SSH logins.

=====================================================================
EOF
fi
