#
# SPDX-License-Identifier: MIT
# Copyright (c) 2020-2024 Tano Systems LLC. All rights reserved.
#

PR:append = ".tano1"

PACKAGECONFIG:remove = "sensord"

do_install:append() {
	# Some applications (e.g. luci-app-statistics) expects sensors in /usr/sbin
	ln -s ${bindir}/sensors ${D}${sbindir}/sensors
}

FILES:${PN}-sensors += "${sbindir}/sensors"
CONFFILES:${PN}-libsensors = "${sysconfdir}/sensors3.conf"

# Do not install fancontrol and pwmconfig utilities
RDEPENDS:${PN}:remove = "${PN}-fancontrol"
RDEPENDS:${PN}:remove = "${PN}-pwmconfig"

# Build always with gcc
TOOLCHAIN = "gcc"
