#
# SPDX-License-Identifier: MIT
# Copyright (c) 2024 Tano Systems LLC. All rights reserved.
#
# A simple class to rename the '.wic' extension to custom for WIC images
#
WIC_IMAGE_CUSTOM_EXTENSION ?= "sdcard.img"

IMAGE_CMD:wic:append () {
	mv "$out${IMAGE_NAME_SUFFIX}.wic" "$out${IMAGE_NAME_SUFFIX}.${WIC_IMAGE_CUSTOM_EXTENSION}"
	ln -fs "${IMAGE_NAME}${IMAGE_NAME_SUFFIX}.${WIC_IMAGE_CUSTOM_EXTENSION}" "${IMGDEPLOYDIR}/${IMAGE_LINK_NAME}.${WIC_IMAGE_CUSTOM_EXTENSION}"
}
