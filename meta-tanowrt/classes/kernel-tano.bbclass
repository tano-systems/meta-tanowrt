#
# SPDX-License-Identifier: MIT
#

inherit tanowrt-kernel-module-split

OBJCOPY:toolchain-clang = "${HOST_PREFIX}objcopy"
STRIP:toolchain-clang = "${HOST_PREFIX}strip"

def kernel_full_version(version):
    parts = version.split("-", 1)
    return parts[0]

def kernel_dtb2dts(var, d):
    value = d.getVar(var, True) or ''
    return value.replace('.dtb', '.dts')

KERNEL_DEVICETREE_COPY ?= ""
KERNEL_DEVICETREE_COPY_DST:mipsarch ?= "${S}/arch/mips/boot/dts"
KERNEL_DEVICETREE_COPY_DST:microblaze ?= "${S}/arch/microblaze/boot/dts"
KERNEL_DEVICETREE_COPY_DST:aarch64 ?= "${S}/arch/arm64/boot/dts"
KERNEL_DEVICETREE_COPY_DST:arm ?= "${S}/arch/arm/boot/dts"

do_configure:append() {
	if [ ! -z "${KERNEL_DEVICETREE_COPY_DST}" ]; then
		for f in ${KERNEL_DEVICETREE_COPY}; do
			bbdebug 1 "${f} (copy ${f} to ${KERNEL_DEVICETREE_COPY_DST}/${f})"
			if [ -e "${KERNEL_DEVICETREE_COPY_DST}/${f}" ]; then
				rm -f ${KERNEL_DEVICETREE_COPY_DST}/${f}.orig
				mv ${KERNEL_DEVICETREE_COPY_DST}/${f} ${KERNEL_DEVICETREE_COPY_DST}/${f}.orig
			fi
			install -D -m 0644 ${WORKDIR}/${f} \
			                   ${KERNEL_DEVICETREE_COPY_DST}/${f}
		done
	fi
}

# LINUX_LOCALVERSION can be set to add a tag to the end of the
# kernel version string. such as the commit id
def get_localversion(d):
    pr = d.getVar("PR")
    srcrev = d.getVar("KERNEL_SRC_SRCREV")
    localversion = ""
    if srcrev and srcrev != "":
        localversion += "-g%s" % (srcrev[:10])
    if pr and pr != "":
        localversion += "-%s" % (pr)

    return localversion

LINUX_LOCALVERSION ?= "${@get_localversion(d)}"

do_configure:append() {
	echo ${LINUX_LOCALVERSION} > ${B}/.scmversion
	echo ${LINUX_LOCALVERSION} > ${S}/.scmversion
}
