#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2022-2024 Tano Systems LLC. All rights reserved.
# Authors: Anton Kikin <a.kikin@tano-systems.com>
#
# Intel Linux kernel 5.10
#
LINUX_INTEL_BRANCH = "5.10/yocto"

KERNEL_SRC_URI ?= "git://github.com/intel/linux-intel-lts.git"
KERNEL_SRC_BRANCH ?= "${LINUX_INTEL_BRANCH}"
KERNEL_SRC_PROTOCOL ?= "https"

# Tag: lts-v5.10.184-yocto-230626T081427Z
KERNEL_SRC_SRCREV ?= "c50f8c5a381297c74a9940132fdb407bc6545f5a"

LINUX_VERSION ?= "5.10.184"
LINUX_KERNEL_TYPE ?= "standard"
PV = "${LINUX_VERSION}+git${SRCPV}"

require recipes-kernel/linux/linux-tano.inc
require recipes-kernel/linux/linux-tano-intel.inc

FILESEXTRAPATHS:prepend := "${THISDIR}/linux-tano-intel-5.10:"

YOCTO_KERNEL_CACHE_BRANCH = "yocto-5.10"
YOCTO_KERNEL_CACHE_SRCREV = "603507f09e4a22a650e37fb9dcfbcb69ceb36841"

SRC_URI:append = "\
	file://0001-menuconfig-mconf-cfg-Allow-specification-of-ncurses-.patch \
	file://0001-io-mapping-Cleanup-atomic-iomap.patch \
"

DEPENDS += "elfutils-native openssl-native util-linux-native"

# For Crystalforest and Romley
KERNEL_MODULE_AUTOLOAD:append:core2-32-intel-common = " uio"
KERNEL_MODULE_AUTOLOAD:append:corei7-64-intel-common = " uio"

KCONF_BSP_AUDIT_LEVEL = "0"

# Disabling CONFIG_SND_SOC_INTEL_SKYLAKE for 32-bit, does not allow to set CONFIG_SND_SOC_INTEL_SST too, which
# causes config warning too.
KCONF_AUDIT_LEVEL:core2-32-intel-common = "0"
