#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2022-2024 Tano Systems LLC. All rights reserved.
# Authors: Anton Kikin <a.kikin@tano-systems.com>
#
# Intel Linux kernel 5.15
#
LINUX_INTEL_BRANCH = "5.15/linux"

KERNEL_SRC_URI ?= "git://github.com/intel/linux-intel-lts.git"
KERNEL_SRC_BRANCH ?= "${LINUX_INTEL_BRANCH}"
KERNEL_SRC_SRCREV ?= "b2769cf869322589ab9147c774404f1f62b6561d"
KERNEL_SRC_PROTOCOL ?= "https"

LINUX_VERSION ?= "5.15.137"
LINUX_KERNEL_TYPE ?= "standard"
PV = "${LINUX_VERSION}+git${SRCPV}"

require recipes-kernel/linux/linux-tano.inc
require recipes-kernel/linux/linux-tano-intel.inc

FILESEXTRAPATHS:prepend := "${THISDIR}/linux-tano-intel-5.15:"

YOCTO_KERNEL_CACHE_BRANCH = "yocto-5.15"
YOCTO_KERNEL_CACHE_SRCREV = "0b002d94afb8a3b60ed1f3be419cb9f5a8815cfc"

SRC_URI:append = "\
	file://0001-v5.15-menuconfig-mconf-cfg-Allow-specification-of-ncurses-.patch \
"

DEPENDS += "elfutils-native openssl-native util-linux-native"

# For Crystalforest and Romley
KERNEL_MODULE_AUTOLOAD:append:core2-32-intel-common = " uio"
KERNEL_MODULE_AUTOLOAD:append:corei7-64-intel-common = " uio"

# Disabling CONFIG_SND_SOC_INTEL_SKYLAKE for 32-bit, does not allow to set CONFIG_SND_SOC_INTEL_SST too, which
# causes config warning too.
KCONF_AUDIT_LEVEL:core2-32-intel-common = "0"
