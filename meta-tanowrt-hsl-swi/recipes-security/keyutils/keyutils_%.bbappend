#
# SPDX-License-Identifier: MIT
# Copyright (c) 2022 Tano Systems LLC. All rights reserved.
#

SRC_URI:remove = "git://git.kernel.org/pub/scm/linux/kernel/git/dhowells/keyutils;protocol=https"
SRC_URI:append = " git://git.kernel.org/pub/scm/linux/kernel/git/dhowells/keyutils;protocol=https;branch=master"

LICENSE = "GPL-2.0-only"
