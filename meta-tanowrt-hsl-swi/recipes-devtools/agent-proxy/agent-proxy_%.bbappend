#
# SPDX-License-Identifier: MIT
# Copyright (c) 2022 Tano Systems LLC. All rights reserved.
#

SRC_URI:remove = "git://git.kernel.org/pub/scm/utils/kernel/kgdb/agent-proxy.git;protocol=git"
SRC_URI:append = " git://git.kernel.org/pub/scm/utils/kernel/kgdb/agent-proxy.git;protocol=git;branch=master"

LICENSE = "GPL-2.0-only"
