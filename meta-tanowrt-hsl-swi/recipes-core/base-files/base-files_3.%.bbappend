#
# SPDX-License-Identifier: MIT
# Copyright (c) 2020 Tano Systems LLC. All rights reserved.
#
PR:append = ".swi0"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/patches:${THISDIR}/${PN}/files:"

PREINIT_SCRIPTS:append = " file://preinit/81_setup_iface_mac "

SRC_URI:append = "\
	file://sysctl.d/40-swi.conf \
	file://eth0_address \
"

do_install:append () {
	install -d ${D}${sysconfdir}/sysctl.d
	install -m 0644 ${WORKDIR}/sysctl.d/40-swi.conf ${D}${sysconfdir}/sysctl.d/
	install -m 0644 ${WORKDIR}/eth0_address ${D}${sysconfdir}/
}

CONFFILES:${PN}:append = "\
	${sysconfdir}/eth0_address \
"
