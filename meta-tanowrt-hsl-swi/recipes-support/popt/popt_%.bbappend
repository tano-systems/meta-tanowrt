#
# SPDX-License-Identifier: MIT
# Copyright (c) 2021 Tano Systems LLC. All rights reserved.
#

PR:append = ".swi0"
FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
