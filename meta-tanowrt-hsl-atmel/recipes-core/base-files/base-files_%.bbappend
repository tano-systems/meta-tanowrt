#
# SPDX-License-Identifier: MIT
# Copyright (c) 2021-2024 Tano Systems LLC. All rights reserved.
#

PR:append:evb-ksz-sd = ".atmel0"
PR:append:at91-sama5d2-xplained-sd = ".atmel0"
PR:append:at91-sama5d2-xplained-emmc = ".atmel0"
PR:append:at91-sama5d3-xplained-sd = ".atmel0"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/files:"

PREINIT_SCRIPTS:append:evb-ksz-sd = " file://preinit/79_mount_boot "
PREINIT_SCRIPTS:append:at91-sama5d2-xplained-sd = " file://preinit/79_mount_boot "
PREINIT_SCRIPTS:append:at91-sama5d2-xplained-emmc = " file://preinit/79_mount_boot "
PREINIT_SCRIPTS:append:at91-sama5d3-xplained-sd = " file://preinit/79_mount_boot "
