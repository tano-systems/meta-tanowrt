#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2024 Tano Systems LLC. All rights reserved.
# Authors: Anton Kikin <a.kikin@tano-systems.com>
#
# Based on original u-boot-mchp_2023.07.bb recipe from meta-atmel layer
#

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://README;beginline=1;endline=22;md5=b5410c33378a67de244a5877f9ff9a27"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}-2023.x:${THISDIR}/${PN}:"

SRCREV = "842d9d53a16ca57a28e618abdf4e714b19632ca1"

PV = "v2023.07-mchp+git${SRCPV}"
PR = "tano0.${INCPR}"

DEPENDS += "coreutils-native bison-native flex-native"

COMPATIBLE_MACHINE = "evb-ksz9477|evb-ksz9563|at91-sama5d2-xplained|at91-sama5d3-xplained"

UBRANCH = "u-boot-2023.07-mchp"

SRC_URI = "git://github.com/linux4microchip/u-boot-mchp.git;protocol=https;branch=${UBRANCH}"

S = "${WORKDIR}/git"

PACKAGE_ARCH = "${MACHINE_ARCH}"

require u-boot-tano-mchp.inc
