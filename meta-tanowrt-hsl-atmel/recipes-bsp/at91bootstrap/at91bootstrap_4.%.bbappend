#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2021-2024 Tano Systems LLC. All rights reserved.
# Authors: Anton Kikin <a.kikin@tano-systems.com>
#

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}-4:${THISDIR}/${PN}:"

COMPATIBLE_MACHINE:append = "|evb-ksz9477|evb-ksz9563|at91-sama5d2-xplained|at91-sama5d3-xplained"

PR:append = ".tano3.${PR_INC}"

require at91bootstrap.inc

AT91BOOTSTRAP_PMECC_HEADER ?= "0"

do_deploy() {
	install -d ${DEPLOYDIR}
	cd ${DEPLOYDIR}

	if [ -n "${AT91BOOTSTRAP_BUILD_CONFIG}" ]; then
		unset i j
		for config in ${AT91BOOTSTRAP_MACHINE}; do
			i=$(expr $i + 1)
			for type in ${AT91BOOTSTRAP_BUILD_CONFIG}; do
				j=$(expr $j + 1);
				if [ $j -eq $i ]; then
					install ${B}/${config}/${AT91BOOTSTRAP_BINARY} \
					        ${DEPLOYDIR}/${AT91BOOTSTRAP_BINARY}-${type}
					rm -f boot.bin-${type} BOOT.BIN-${type}
					ln -sf ${AT91BOOTSTRAP_BINARY}-${type} boot.bin-${type}

					if [ "${AT91BOOTSTRAP_PMECC_HEADER}" = "1" ]; then
						# Generate binary with PMECC header
						cd ${S}/scripts && python3 addpmecchead.py \
							"${B}/${config}/.config" "${DEPLOYDIR}/"

						rm -f boot.bin-${type}-pmecc BOOT.BIN-${type}-pmecc
						rm -f "${DEPLOYDIR}/${AT91BOOTSTRAP_BINARY}-${type}-pmecc"

						if [ -f "${DEPLOYDIR}/pmecc.tmp" ]; then
							cat "${DEPLOYDIR}/pmecc.tmp" \
								"${B}/${config}/${AT91BOOTSTRAP_BINARY}" \
								> "${DEPLOYDIR}/${AT91BOOTSTRAP_BINARY}-${type}-pmecc"
							rm -f "${DEPLOYDIR}/pmecc.tmp"
						else
							cp -vf "${B}/${config}/${AT91BOOTSTRAP_BINARY}" \
							       "${DEPLOYDIR}/${AT91BOOTSTRAP_BINARY}-${type}-pmecc"
						fi

						ln -sf ${AT91BOOTSTRAP_BINARY}-${type}-pmecc boot.bin-${type}-pmecc
					fi
				fi
			done
			unset j
		done
		unset i
	else
		install ${S}/${AT91BOOTSTRAP_BIN_PATH}/${AT91BOOTSTRAP_BINARY} ${DEPLOYDIR}/${AT91BOOTSTRAP_IMAGE}

		rm -f ${AT91BOOTSTRAP_BINARY} ${AT91BOOTSTRAP_SYMLINK}
		ln -sf ${AT91BOOTSTRAP_IMAGE} ${AT91BOOTSTRAP_SYMLINK}
		ln -sf ${AT91BOOTSTRAP_IMAGE} ${AT91BOOTSTRAP_BINARY}

		# Create a symlink ready for file copy on SD card
		rm -f boot.bin BOOT.BIN
		ln -sf ${AT91BOOTSTRAP_IMAGE} BOOT.BIN
	fi
}
