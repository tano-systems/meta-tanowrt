#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2021 Tano Systems LLC. All rights reserved.
# Authors: Anton Kikin <a.kikin@tano-systems.com>
#

PR:append = ".0"

COMPATIBLE_MACHINE = "(sama5)"

KERNEL_VERSION_SANITY_SKIP = "1"

FILESEXTRAPATHS:prepend = "${TANOWRT_HSL_BASE}/recipes-kernel/linux/features:"

KERNEL_DEVICETREE_COPY = "${@kernel_dtb2dts('KERNEL_DEVICETREE', d)}"

KERNEL_MACHINE_INCLUDE:evb-ksz9477 ?= "recipes-kernel/linux/linux-tano-mchp-evb-ksz9477.inc"
KERNEL_MACHINE_INCLUDE:evb-ksz9563 ?= "recipes-kernel/linux/linux-tano-mchp-evb-ksz9563.inc"
KERNEL_MACHINE_INCLUDE:at91-sama5d2-xplained ?= "recipes-kernel/linux/linux-tano-mchp-sama5d2-xplained.inc"
KERNEL_MACHINE_INCLUDE:at91-sama5d3-xplained ?= "recipes-kernel/linux/linux-tano-mchp-sama5d3-xplained.inc"
KERNEL_MACHINE_INCLUDE ?= "recipes-kernel/linux/linux-tano-mchp-${MACHINE}.inc"
include ${KERNEL_MACHINE_INCLUDE}

KERNEL_MODULE_AUTOLOAD += "atmel_usba_udc g_serial"

LINUX_VERSION_EXTENSION = "-tano-mchp-${LINUX_KERNEL_TYPE}"
