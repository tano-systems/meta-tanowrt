#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2022-2024 Tano Systems LLC. All rights reserved.
# Authors: Anton Kikin <a.kikin@tano-systems.com>
#

PR:append = ".1"

KCONFIG_MODE = "--alldefconfig"

# Config
SRC_URI:append = " file://defconfig"

# Using devicetree from kernel sources
KERNEL_DEVICETREE_COPY = ""
