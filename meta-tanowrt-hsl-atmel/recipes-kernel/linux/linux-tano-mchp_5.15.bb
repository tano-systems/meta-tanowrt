#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2024 Tano Systems LLC. All rights reserved.
# Authors: Anton Kikin <a.kikin@tano-systems.com>
#
# Linux kernel for Microchip ARM SoCs (aka AT91)
#

SECTION = "kernel"
DESCRIPTION = "Linux kernel for Microchip ARM SoCs (aka AT91)"
SUMMARY = "Linux kernel for Microchip ARM SoCs (aka AT91)"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

KERNEL_SRC_URI ?= "git://github.com/linux4microchip/linux.git"
KERNEL_SRC_BRANCH ?= "linux-5.15-mchp"
KERNEL_SRC_PROTOCOL ?= "https"
KERNEL_SRC_SRCREV ?= "03bed3baddfded16d7869a7251296237b2af6842"

LINUX_VERSION ?= "5.15.105"
LINUX_KERNEL_TYPE ?= "standard"
PV = "${LINUX_VERSION}+git${SRCPV}"

# Append to the MACHINE_KERNEL_PR so that a new SRCREV will cause a rebuild
MACHINE_KERNEL_PR:append = "tano0"
PR = "${MACHINE_KERNEL_PR}"

require recipes-kernel/linux/linux-tano.inc
require recipes-kernel/linux/linux-tano-mchp.inc

LINUX_VERSION_EXTENSION = "-tano-mchp-${LINUX_KERNEL_TYPE}"

# Look in the generic major.minor directory for files
FILESEXTRAPATHS:prepend := "${THISDIR}/linux-tano-mchp-5.15/files:"
FILESEXTRAPATHS:prepend := "${THISDIR}/linux-tano-mchp-5.15/patches:"
FILESEXTRAPATHS:prepend := "${THISDIR}/linux-tano-mchp-5.15/devicetree:"

SRC_URI:append = "\
	file://0001-ubi-Fix-failure-attaching-when-vid_hdr-offset-equals-to-(sub)page-size.patch \
	file://0002-spi-atmel-Fix-clock-issue-when-using-devices-with-different-polarities.patch \
"
