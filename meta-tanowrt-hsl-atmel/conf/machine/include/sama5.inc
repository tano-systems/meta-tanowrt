# Atmel SAMA5 defaults
require conf/machine/include/soc-family.inc
require conf/machine/include/arm/armv7a/tune-cortexa5.inc

# Add bootloaders to the images of every machine
EXTRA_IMAGEDEPENDS += "at91bootstrap virtual/bootloader"

SOC_FAMILY = "sama5"

PREFERRED_PROVIDER_virtual/kernel:sama5 = "linux-tano-mchp"
PREFERRED_PROVIDER_virtual/bootloader:sama5 ?= "u-boot-tano-mchp"
PREFERRED_PROVIDER_u-boot:sama5 ?= "u-boot-tano-mchp"

PREFERRED_VERSION_at91bootstrap = "4.0.8%"
PREFERRED_VERSION_u-boot-tano-mchp = "v2023.07%"

PREFERRED_VERSION_linux-tano-mchp = "6.1%"
LINUXLIBCVERSION = "6.1%"
SDKLINUXLIBCVERSION = "6.1%"
TANOWRT_WIREGUARD_IN_KERNEL = "1"

PREFERRED_PROVIDER_jpeg ?= "jpeg"
PREFERRED_PROVIDER_jpeg-native ?= "jpeg-native"

SERIAL_CONSOLES ?= "115200;ttyS0"
WIC_CREATE_EXTRA_ARGS ?= "--no-fstab-update"
