#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2022-2023 Tano Systems LLC. All rights reserved.
# Authors: Anton Kikin <a.kikin@tano-systems.com>
#
# Linux kernel for Rockchip SoC's
#

SECTION = "kernel"
DESCRIPTION = "Linux kernel 4.19 for Rockchip SoC's"
SUMMARY = "Linux kernel 4.19 for Rockchip SoC's"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

KERNEL_SRC_URI ?= "git://github.com/JeffyCN/mirrors.git"
KERNEL_SRC_BRANCH ?= "kernel-4.19-2022_11_23"
KERNEL_SRC_NOBRANCH ?= "1"
KERNEL_SRC_PROTOCOL ?= "https"
KERNEL_SRC_SRCREV ?= "f39b590ea11da2d897644e42119ba5099b2eefc8"

LINUX_VERSION ?= "4.19.232"
LINUX_KERNEL_TYPE ?= "standard"
PV = "${LINUX_VERSION}+git${SRCPV}"

# Append to the MACHINE_KERNEL_PR so that a new SRCREV will cause a rebuild
MACHINE_KERNEL_PR:append = "tano3"
PR = "${MACHINE_KERNEL_PR}"

require recipes-kernel/linux/linux-tano.inc
require recipes-kernel/linux/linux-tano-rockchip.inc

COMPATIBLE_MACHINE = "(boardcon-em3566|boardcon-em3568|rock-pi-s)"

# Look in the generic major.minor directory for files
FILESEXTRAPATHS:prepend := "${THISDIR}/linux-tano-rockchip-4.19/files:"
FILESEXTRAPATHS:prepend := "${THISDIR}/linux-tano-rockchip-4.19/patches:"
FILESEXTRAPATHS:prepend := "${THISDIR}/linux-tano-rockchip-4.19/devicetree:"
FILESEXTRAPATHS:prepend := "${THISDIR}/linux-tano-rockchip-4.19/features:"

SRC_URI:append = "\
	file://config-fixup.scc \
"

KERNEL_FEATURES:append = "\
	config-fixup.scc \
"

SRC_URI += "\
	file://0006-HACK-drm-rockchip-Force-enable-legacy-cursor-update.patch \
	file://0007-HACK-drm-rockchip-Prefer-non-cluster-overlay-planes.patch \
	file://1000-arm64_increasing_DMA_block_memory_allocation_to_2048.patch \
	file://1001-rk3308-Fix-main-dtsi.patch \
	file://1002-Build-overlays.patch \
	file://1003-GPIO-add-named-gpio-exports.patch \
	file://1004-rtc-hym8563-Add-updated-by-Rockchip-driver.patch \
	file://1005-rtkbtusb-Add-modified-Rockchip-driver.patch \
	file://1006-rtl8723du-Add-driver.patch \
"

# RK3308BS support patches
RK3308BS_SUPPORT_PATCHES = "\
	file://1300-arm64-dts-rockchip-add-rk3308bs-pinctrl-fix.patch \
	file://1301-arm64-dts-rockchip-fix-rk3308bs-pinctrl-pcfg-error.patch \
	file://1302-arm64-dts-rockchip-optimize-the-pinctrl-of-LCDC-for-.patch \
	file://1303-arm64-dts-rockchip-fix-the-pinctrl-of-LCDC-for-rk330.patch \
	file://1304-arm64-dts-rockchip-Add-opp-table-for-rk3308bs.patch \
	file://1305-arm64-dts-rockchip-Modify-cpu-opp-table-for-rk3308bs.patch \
	file://1306-arm64-dts-rockchip-rk3308-Change-maximum-voltage-for.patch \
	file://1307-soc-rockchip-add-cpuid-for-rk3308bs.patch \
	file://1308-clk-rockchip-rk3308-remove-unnecessary-DDR-clocks.patch \
	file://1309-pinctrl-rockchip-Add-rk3308b-s-support.patch \
	file://1310-pinctrl-rockchip-add-DRV_TYPE_IO_SMIC.patch \
	file://1311-pinctrl-rockchip-support-smic-io-type.patch \
	file://1312-nvmem-rockchip-otp-Add-support-for-rk3308bs-otp.patch \
	file://1313-PM-AVS-rockchip-iodomain-dump-domain-setting.patch \
	file://1314-phy-rockchip-inno-usb2-support-rk3308bs-tuning.patch \
	file://1315-phy-rockchip-inno-usb2-fix-rk3308bs-tuning.patch \
	file://1316-phy-rockchip-inno_usb2-tuning-phy-squelch-trigger-to.patch \
	file://1317-dt-bindings-suspend-rk3308-add-pwm-regulator-voltage.patch \
	file://1318-ASoC-rk3308_codec-update-some-codec-features-for-RK3.patch \
	file://1319-ASoC-rk3308_codec-recover-default-0dB-gain-for-DAC.patch \
	file://1320-ASoC-rk3308_codec-clean-up-unnecessary-AGC-controls.patch \
	file://1321-ASoC-rk3308_codec-fix-the-DAC-Digital-gain-for-RK330.patch \
	file://1322-ASoC-rk3308_codec-fix-the-leak-LINEOUT-signal-when-u.patch \
	file://1323-ASoC-rk3308_codec-Using-large-driver-strength-for-HP.patch \
	file://1324-ASoC-rk3308_codec-keep-DAC-mclk-enabled-for-RK3308BS.patch \
	file://1325-ASoC-rk3308_codec-fix-codec-no-sound-when-drop-testi.patch \
	file://1326-thermal-rockchip-Support-the-rk3308bs-SoC-in-thermal.patch \
	file://1327-rk3308bs-usb-device-fix-abnormal-linestate.patch \
	file://1328-rk3308bs-usb-dwc2-host-fix-abnormal-linestate.patch \
	file://1329-rk3308bs-usb-ehci-host-fix-abnormal-linestate.patch \
	file://1330-phy-rockchip-inno-usb2-keep-utmi-clk-on-during-charg.patch \
"

SRC_URI += "${RK3308BS_SUPPORT_PATCHES}"

# Not required for 4.19.232
SRC_URI:remove = "file://550-loop-better-discard-for-block-devices.patch"

# This 4.19 kernel has backported wireguard in-kernel support
KERNEL_FEATURES:append = " features/kernel-5.6+/wireguard/wireguard.scc"
