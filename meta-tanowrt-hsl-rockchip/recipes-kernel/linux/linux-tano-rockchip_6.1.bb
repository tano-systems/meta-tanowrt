#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2024 Tano Systems LLC. All rights reserved.
# Authors: Anton Kikin <a.kikin@tano-systems.com>
#
# Linux kernel for Rockchip SoC's
#

SECTION = "kernel"
DESCRIPTION = "Linux kernel 6.1 for Rockchip SoC's"
SUMMARY = "Linux kernel 6.1 for Rockchip SoC's"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

#KERNEL_SRC_URI ?= "git://github.com/friendlyarm/kernel-rockchip"
#KERNEL_SRC_BRANCH ?= "nanopi6-v6.1.y"
#KERNEL_SRC_NOBRANCH ?= "0"
#KERNEL_SRC_PROTOCOL ?= "https"
#KERNEL_SRC_SRCREV ?= "7c605af716d51f970797d1e0b6885e2b9b741be7"

KERNEL_SRC_URI ?= "git://github.com/JeffyCN/mirrors.git"
KERNEL_SRC_BRANCH ?= "kernel-6.1"
KERNEL_SRC_NOBRANCH ?= "0"
KERNEL_SRC_PROTOCOL ?= "https"
KERNEL_SRC_SRCREV ?= "ea9e2a9344bfe7f1130dee8100173b6cb95445d2"

LINUX_VERSION ?= "6.1.57"
LINUX_KERNEL_TYPE ?= "standard"
PV = "${LINUX_VERSION}+git${SRCPV}"

# Append to the MACHINE_KERNEL_PR so that a new SRCREV will cause a rebuild
MACHINE_KERNEL_PR:append = "tano0"
PR = "${MACHINE_KERNEL_PR}"

require recipes-kernel/linux/linux-tano.inc
require recipes-kernel/linux/linux-tano-rockchip.inc

COMPATIBLE_MACHINE = "(nanopi-r5c)"

# Look in the generic major.minor directory for files
FILESEXTRAPATHS:prepend := "${THISDIR}/linux-tano-rockchip-6.1/files:"
FILESEXTRAPATHS:prepend := "${THISDIR}/linux-tano-rockchip-6.1/patches:"
FILESEXTRAPATHS:prepend := "${THISDIR}/linux-tano-rockchip-6.1/devicetree:"
FILESEXTRAPATHS:prepend := "${THISDIR}/linux-tano-rockchip-6.1/features:"

SRC_URI:append = "\
	file://config-fixup.scc \
"

KERNEL_FEATURES:append = "\
	config-fixup.scc \
"

SRC_URI += "\
	file://0001-init-do_mounts.c-Retry-all-fs-after-failed-to-mount-.patch \
	file://0002-HACK-drm-rockchip-Force-enable-legacy-cursor-update.patch \
	file://0003-HACK-drm-rockchip-Prefer-non-cluster-overlay-planes.patch \
	file://1002-Build-overlays.patch \
	file://1003-GPIO-add-named-gpio-exports.patch \
	file://1004-rtc-hym8563-Add-updated-by-Rockchip-driver.patch \
"
