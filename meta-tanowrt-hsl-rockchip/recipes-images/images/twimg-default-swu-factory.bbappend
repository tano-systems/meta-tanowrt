#
# SPDX-License-Identifier: MIT
# Copyright (c) 2022, 2024 Tano Systems LLC. All rights reserved.
#

COMPATIBLE_MACHINE:append = "|boardcon-em3566-emmc|boardcon-em3568-emmc|rock-pi-s-sdnand|nanopi-r5c-emmc"
