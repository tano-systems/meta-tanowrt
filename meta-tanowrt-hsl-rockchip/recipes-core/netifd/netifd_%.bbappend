#
# SPDX-License-Identifier: MIT
# Copyright (c) 2021, 2024 Tano Systems LLC. All rights reserved.
#
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/files:"

PR:append = ".rk0"

TANOWRT_NETIFD_NETWORK_CONFIG = "0"

NET_HOTPLUG_SCRIPTS:append:nanopi-r5c = " file://rootfs/etc/hotplug.d/net/40-net-smp-affinity "
