#
# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2024 Tano Systems LLC
#

# rockchip_rk3568-nanopi5-rev%02x.dtb
setenv bootconfigs "#conf-rockchip_rk3568-nanopi5-rev${board_rev}.dtb";
