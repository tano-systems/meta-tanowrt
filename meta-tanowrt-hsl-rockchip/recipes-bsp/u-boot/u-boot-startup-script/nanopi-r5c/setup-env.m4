# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2024 Tano Systems LLC

setenv optargs "${optargs} cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory coherent_pool=1m"
