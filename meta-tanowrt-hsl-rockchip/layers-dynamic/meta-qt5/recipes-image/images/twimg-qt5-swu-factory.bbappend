# SPDX-License-Identifier: MIT
# Copyright (C) 2024 Anton Kikin <a.kikin@tano-systems.com>

COMPATIBLE_MACHINE:append = "|boardcon-em3566-emmc|boardcon-em3568-emmc|nanopi-r5c-emmc"
