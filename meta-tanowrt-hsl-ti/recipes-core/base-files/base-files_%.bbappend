#
# SPDX-License-Identifier: MIT
# Copyright (c) 2020 Tano Systems LLC. All rights reserved.
#
PR:append:am335x-icev2 = ".ti0"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/patches:${THISDIR}/${PN}/files:"

PREINIT_SCRIPTS:append:am335x-icev2 = "\
	file://rootfs/lib/preinit/01_display_lcd_message \
"
