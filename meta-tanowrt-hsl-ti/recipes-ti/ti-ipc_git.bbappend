#
# SPDX-License-Identifier: MIT
# Copyright (c) 2020 Tano Systems LLC. All rights reserved.
#
PR:append:am574x-idk = ".tano"
ALT_PLATFORM:am574x-idk = "AM572X AM571X"
