.. table:: Supported Intel CPU Based Boards
   :width: 100%
   :widths: 20, 30, 10, 10, 10, 20

   +---------------------------------------------------+----------------------------+-------------+-------------------------------------+----------------------+----------------------------------+
   | Board                                             | SoC                        | RAM         | Supported Kernel Version(s)         | Supported Storage(s) | Target YAML                      |
   |                                                   |                            |             |                                     |                      | (at :file:`kas/targets`)         |
   +===================================================+============================+=============+=====================================+======================+==================================+
   | Intel Core i7 CPU (and later)                     | x86_64 (Intel Core i7)     | At least    || **5.15.137 (non-RT) [Default]**    || USB                 | |intel-x86-64-corei7.yml|        |
   +---------------------------------------------------+----------------------------+ 64 MiB      || 5.15.137-rt71 (RT)                 || SATA                +----------------------------------+
   | Intel Skylake CPU (and later)                     | x86_64 (Intel Skylake)     |             || 5.10.184 (non-RT)                  || NVMe                | |intel-x86-64-skylake.yml|       |
   |                                                   |                            |             || 5.10.184-rt90 (RT)                 |                      |                                  |
   +---------------------------------------------------+----------------------------+-------------+-------------------------------------+----------------------+----------------------------------+

.. |intel-x86-64-corei7.yml|  replace:: :tanowrt_git_blob:`intel-x86-64-corei7.yml  </kas/targets/intel-x86-64-corei7.yml>`
.. |intel-x86-64-skylake.yml| replace:: :tanowrt_git_blob:`intel-x86-64-skylake.yml </kas/targets/intel-x86-64-skylake.yml>`
