[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Linux version 6.1.74-tano-mchp-standard-g3f74b634cc-tano0.2.1.20.0.1 (oe-user@oe-host) (arm-oe-linux-gnueabi-gcc (GCC) 11.4.0, GNU ld (GNU Binutils) 2.38.20220708) #1 Tue Jan 23 16:07:43 UTC 2024
[    0.000000] CPU: ARMv7 Processor [410fc051] revision 1 (ARMv7), cr=10c53c7d
[    0.000000] CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
[    0.000000] OF: fdt: Machine model: SAMA5D3 Xplained
[    0.000000] Memory policy: Data cache writeback
[    0.000000] cma: Reserved 16 MiB at 0x2e800000
[    0.000000] Zone ranges:
[    0.000000]   Normal   [mem 0x0000000020000000-0x000000002fffffff]
[    0.000000] Movable zone start for each node
[    0.000000] Early memory node ranges
[    0.000000]   node   0: [mem 0x0000000020000000-0x000000002fffffff]
[    0.000000] Initmem setup node 0 [mem 0x0000000020000000-0x000000002fffffff]
[    0.000000] CPU: All CPU(s) started in SVC mode.
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 64960
[    0.000000] Kernel command line: console=ttyS0,115200n8  root=/dev/mmcblk0p3 ro rootfstype=squashfs rootwait rootfs_partition=3 rootfs_volume=3 earlyprintk panic=15
[    0.000000] Unknown kernel command line parameters "earlyprintk rootfs_partition=3 rootfs_volume=3", will be passed to user space.
[    0.000000] Dentry cache hash table entries: 32768 (order: 5, 131072 bytes, linear)
[    0.000000] Inode-cache hash table entries: 16384 (order: 4, 65536 bytes, linear)
[    0.000000] mem auto-init: stack:off, heap alloc:off, heap free:off
[    0.000000] Memory: 229084K/262144K available (9216K kernel code, 461K rwdata, 2336K rodata, 1024K init, 168K bss, 16676K reserved, 16384K cma-reserved)
[    0.000000] NR_IRQS: 16, nr_irqs: 16, preallocated irqs: 16
[    0.000000] clocksource: timer@f0010000: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 115833966437 ns
[    0.000004] sched_clock: 32 bits at 17MHz, resolution 60ns, wraps every 130150523873ns
[    0.000043] Switching to timer-based delay loop, resolution 60ns
[    0.000470] clocksource: pit: mask: 0xfffffff max_cycles: 0xfffffff, max_idle_ns: 14479245754 ns
[    0.001178] Console: colour dummy device 80x30
[    0.001268] Calibrating delay loop (skipped), value calculated using timer frequency.. 33.00 BogoMIPS (lpj=165000)
[    0.001306] CPU: Testing write buffer coherency: ok
[    0.001395] pid_max: default: 32768 minimum: 301
[    0.001827] Mount-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
[    0.001882] Mountpoint-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
[    0.005720] Setting up static identity map for 0x20100000 - 0x20100060
[    0.007155] devtmpfs: initialized
[    0.022657] VFP support v0.3: implementor 41 architecture 2 part 30 variant 5 rev 1
[    0.023184] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604462750000 ns
[    0.023238] futex hash table entries: 256 (order: -1, 3072 bytes, linear)
[    0.025483] pinctrl core: initialized pinctrl subsystem
[    0.028488] NET: Registered PF_NETLINK/PF_ROUTE protocol family
[    0.030893] DMA: preallocated 256 KiB pool for atomic coherent allocations
[    0.033664] cpuidle: using governor menu
[    0.066972] AT91: PM: standby: standby, suspend: ulp0
[    0.068781] gpio-at91 fffff200.gpio: at address (ptrval)
[    0.070568] gpio-at91 fffff400.gpio: at address (ptrval)
[    0.072421] gpio-at91 fffff600.gpio: at address (ptrval)
[    0.074372] gpio-at91 fffff800.gpio: at address (ptrval)
[    0.076430] gpio-at91 fffffa00.gpio: at address (ptrval)
[    0.078429] pinctrl-at91 ahb:apb:pinctrl@fffff200: initialized AT91 pinctrl driver
[    0.105010] at_hdmac ffffe600.dma-controller: Atmel AHB DMA Controller ( cpy set slave ), 8 channels
[    0.107238] at_hdmac ffffe800.dma-controller: Atmel AHB DMA Controller ( cpy set slave ), 8 channels
[    0.110257] AT91: Detected SoC family: sama5d3
[    0.110280] AT91: Detected SoC: sama5d36, revision 2
[    0.112574] SCSI subsystem initialized
[    0.113245] usbcore: registered new interface driver usbfs
[    0.113391] usbcore: registered new interface driver hub
[    0.113512] usbcore: registered new device driver usb
[    0.114727] at91_i2c f0014000.i2c: using dma0chan0 (tx) and dma0chan1 (rx) for DMA transfers
[    0.115130] i2c i2c-0: using pinctrl states for GPIO recovery
[    0.115243] i2c i2c-0: using generic GPIOs for recovery
[    0.115384] at91_i2c f0014000.i2c: AT91 i2c bus driver (hw version: 0x402).
[    0.116366] at91_i2c f0018000.i2c: using dma0chan2 (tx) and dma0chan3 (rx) for DMA transfers
[    0.116742] i2c i2c-1: using pinctrl states for GPIO recovery
[    0.116858] i2c i2c-1: using generic GPIOs for recovery
[    0.117000] at91_i2c f0018000.i2c: AT91 i2c bus driver (hw version: 0x402).
[    0.117951] at91_i2c f801c000.i2c: can't get DMA channel, continue without DMA support
[    0.118336] i2c i2c-2: using pinctrl states for GPIO recovery
[    0.118450] i2c i2c-2: using generic GPIOs for recovery
[    0.118588] at91_i2c f801c000.i2c: AT91 i2c bus driver (hw version: 0x402).
[    0.119459] mc: Linux media interface: v0.10
[    0.119638] videodev: Linux video capture interface: v2.00
[    0.119811] pps_core: LinuxPPS API ver. 1 registered
[    0.119825] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    0.119886] PTP clock support registered
[    0.120695] Advanced Linux Sound Architecture Driver Initialized.
[    0.121979] Bluetooth: Core ver 2.22
[    0.122132] NET: Registered PF_BLUETOOTH protocol family
[    0.122148] Bluetooth: HCI device and connection manager initialized
[    0.122179] Bluetooth: HCI socket layer initialized
[    0.122210] Bluetooth: L2CAP socket layer initialized
[    0.122261] Bluetooth: SCO socket layer initialized
[    0.123761] clocksource: Switched to clocksource timer@f0010000
[    0.156261] NET: Registered PF_INET protocol family
[    0.156735] IP idents hash table entries: 4096 (order: 3, 32768 bytes, linear)
[    0.158563] tcp_listen_portaddr_hash hash table entries: 1024 (order: 0, 4096 bytes, linear)
[    0.158650] Table-perturb hash table entries: 65536 (order: 6, 262144 bytes, linear)
[    0.158694] TCP established hash table entries: 2048 (order: 1, 8192 bytes, linear)
[    0.158766] TCP bind hash table entries: 2048 (order: 2, 16384 bytes, linear)
[    0.158868] TCP: Hash tables configured (established 2048 bind 2048)
[    0.159101] UDP hash table entries: 256 (order: 0, 4096 bytes, linear)
[    0.159157] UDP-Lite hash table entries: 256 (order: 0, 4096 bytes, linear)
[    0.159540] NET: Registered PF_UNIX/PF_LOCAL protocol family
[    0.160782] RPC: Registered named UNIX socket transport module.
[    0.160812] RPC: Registered udp transport module.
[    0.160821] RPC: Registered tcp transport module.
[    0.160829] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    0.162293] Initialise system trusted keyrings
[    0.163821] workingset: timestamp_bits=14 max_order=16 bucket_order=2
[    0.164866] squashfs: version 4.0 (2009/01/31) Phillip Lougher
[    0.367627] Key type asymmetric registered
[    0.367662] Asymmetric key parser 'x509' registered
[    0.367823] io scheduler mq-deadline registered
[    0.367849] io scheduler kyber registered
[    0.390476] brd: module loaded
[    0.406377] loop: module loaded
[    0.408032] atmel_usart_serial.0.auto: ttyS1 at MMIO 0xf001c000 (irq = 28, base_baud = 4125000) is a ATMEL_SERIAL
[    0.410727] atmel_usart_serial.1.auto: ttyS2 at MMIO 0xf0020000 (irq = 29, base_baud = 4125000) is a ATMEL_SERIAL
[    0.413363] atmel_usart_serial.2.auto: ttyS5 at MMIO 0xf0024000 (irq = 30, base_baud = 4125000) is a ATMEL_SERIAL
[    0.416149] atmel_usart_serial.3.auto: ttyS0 at MMIO 0xffffee00 (irq = 31, base_baud = 8250000) is a ATMEL_SERIAL
[    1.112734] printk: console [ttyS0] enabled
[    1.122744] atmel_spi f0004000.spi: Using dma0chan4 (tx) and dma0chan5 (rx) for DMA transfers
[    1.132616] atmel_spi f0004000.spi: Atmel SPI Controller version 0x213 at 0xf0004000 (irq 32)
[    1.142278] atmel_spi f8008000.spi: Using dma1chan0 (tx) and dma1chan1 (rx) for DMA transfers
[    1.152018] atmel_spi f8008000.spi: Atmel SPI Controller version 0x213 at 0xf8008000 (irq 33)
[    1.164226] CAN device driver interface
[    1.170026] at91_can f000c000.can: device registered (reg_base=(ptrval), irq=34)
[    1.184057] macb f0028000.ethernet eth0: Cadence GEM rev 0x00020119 at 0xf0028000 irq 35 (00:10:a1:98:97:01)
[    1.199317] macb f802c000.ethernet eth1: Cadence MACB rev 0x0001010c at 0xf802c000 irq 36 (00:10:a1:98:97:02)
[    1.209905] PPP generic driver version 2.4.2
[    1.215182] PPP MPPE Compression module registered
[    1.219969] NET: Registered PF_PPPOX protocol family
[    1.229508] atmel-ehci 700000.ehci: EHCI Host Controller
[    1.234952] atmel-ehci 700000.ehci: new USB bus registered, assigned bus number 1
[    1.242965] atmel-ehci 700000.ehci: irq 37, io mem 0x00700000
[    1.273833] atmel-ehci 700000.ehci: USB 2.0 started, EHCI 1.00
[    1.280051] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002, bcdDevice= 6.01
[    1.288392] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.295640] usb usb1: Product: EHCI Host Controller
[    1.300492] usb usb1: Manufacturer: Linux 6.1.74-tano-mchp-standard-g3f74b634cc-tano0.2.1.20.0.1 ehci_hcd
[    1.310124] usb usb1: SerialNumber: 700000.ehci
[    1.316394] hub 1-0:1.0: USB hub found
[    1.320227] hub 1-0:1.0: 3 ports detected
[    1.328238] at91_ohci 600000.ohci: USB Host Controller
[    1.333433] at91_ohci 600000.ohci: new USB bus registered, assigned bus number 2
[    1.341338] at91_ohci 600000.ohci: irq 37, io mem 0x00600000
[    1.418213] usb usb2: New USB device found, idVendor=1d6b, idProduct=0001, bcdDevice= 6.01
[    1.426558] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.433802] usb usb2: Product: USB Host Controller
[    1.438567] usb usb2: Manufacturer: Linux 6.1.74-tano-mchp-standard-g3f74b634cc-tano0.2.1.20.0.1 ohci_hcd
[    1.448222] usb usb2: SerialNumber: at91
[    1.453645] hub 2-0:1.0: USB hub found
[    1.457568] hub 2-0:1.0: 3 ports detected
[    1.463862] usbcore: registered new interface driver cdc_acm
[    1.469513] cdc_acm: USB Abstract Control Model driver for USB modems and ISDN adapters
[    1.477845] usbcore: registered new interface driver usb-storage
[    1.484175] usbcore: registered new interface driver usbserial_generic
[    1.490767] usbserial: USB Serial support registered for generic
[    1.496920] usbcore: registered new interface driver ftdi_sio
[    1.502711] usbserial: USB Serial support registered for FTDI USB Serial Device
[    1.513181] at91_rtc fffffeb0.rtc: registered as rtc0
[    1.518354] at91_rtc fffffeb0.rtc: setting system clock to 2007-01-01T00:00:09 UTC (1167609609)
[    1.527159] at91_rtc fffffeb0.rtc: AT91 Real Time Clock driver.
[    1.533390] i2c_dev: i2c /dev entries driver
[    1.540844] at91-reset fffffe00.reset-controller: Starting after wakeup
[    1.549383] Bluetooth: HCI UART driver ver 2.3
[    1.553892] Bluetooth: HCI UART protocol H4 registered
[    1.559100] Bluetooth: HCI UART protocol Three-wire (H5) registered
[    1.565574] usbcore: registered new interface driver btusb
[    1.571648] sdhci: Secure Digital Host Controller Interface driver
[    1.577895] sdhci: Copyright(c) Pierre Ossman
[    1.582515] sdhci-pltfm: SDHCI platform and OF driver helper
[    1.590346] ledtrig-cpu: registered to indicate activity on CPUs
[    1.596993] atmel_aes f8038000.crypto: version: 0x135
[    1.602360] atmel_aes f8038000.crypto: Atmel AES - Using dma1chan2, dma1chan3 for DMA transfers
[    1.612019] atmel_sha f8034000.crypto: version: 0x410
[    1.617232] atmel_sha f8034000.crypto: using dma1chan4 for DMA transfers
[    1.624095] atmel_sha f8034000.crypto: Atmel SHA1/SHA256/SHA224/SHA384/SHA512
[    1.632098] atmel_tdes f803c000.crypto: version: 0x701
[    1.637518] atmel_tdes f803c000.crypto: using dma1chan5, dma1chan6 for DMA transfers
[    1.645562] atmel_tdes f803c000.crypto: Atmel DES/TDES
[    1.651443] usbcore: registered new interface driver usbhid
[    1.657095] usbhid: USB HID core driver
[    1.665051] nand: device found, Manufacturer ID: 0x2c, Chip ID: 0xda
[    1.671410] nand: Micron MT29F2G08ABAEAWP
[    1.675499] nand: 256 MiB, SLC, erase size: 128 KiB, page size: 2048, OOB size: 64
[    1.685119] atmel_mci f0000000.mmc: version: 0x505
[    1.690000] atmel_mci f0000000.mmc: using dma0chan7 for DMA transfers
[    1.699016] atmel_mci f8000000.mmc: version: 0x505
[    1.703981] atmel_mci f8000000.mmc: using dma1chan7 for DMA transfers
[    1.711746] Bad block table found at page 131008, version 0x01
[    1.718464] Bad block table found at page 130944, version 0x01
[    1.724827] atmel_mci f0000000.mmc: Atmel MCI controller at 0xf0000000 irq 43, 1 slots
[    1.734233] 7 fixed-partitions partitions found on MTD device atmel_nand
[    1.740990] Creating 7 MTD partitions on "atmel_nand":
[    1.746187] 0x000000000000-0x000000040000 : "at91bootstrap"
[    1.752433] atmel_mci f8000000.mmc: Atmel MCI controller at 0xf8000000 irq 44, 1 slots
[    1.764740] 0x000000040000-0x000000180000 : "uboot"
[    1.773359] 0x000000180000-0x0000001c0000 : "startup"
[    1.782314] 0x0000001c0000-0x000000200000 : "ubootenv"
[    1.791186] 0x000000200000-0x000006a00000 : "system_a"
[    1.800792] 0x000006a00000-0x00000d200000 : "system_b"
[    1.810055] 0x00000d200000-0x000010000000 : "rootfs_data"
[    1.821162] iio iio:device0: Resolution used: 12 bits
[    1.826299] iio iio:device0: ADC Touch screen is disabled.
[    1.838834] u32 classifier
[    1.841512]     input device check on
[    1.845259]     Actions configured
[    1.849278] NET: Registered PF_INET6 protocol family
[    1.858035] Segment Routing with IPv6
[    1.861896] In-situ OAM (IOAM) with IPv6
[    1.866083] sit: IPv6, IPv4 and MPLS over IPv4 tunneling driver
[    1.873551] NET: Registered PF_PACKET protocol family
[    1.878822] Bridge firewalling registered
[    1.882803] can: controller area network core
[    1.887415] NET: Registered PF_CAN protocol family
[    1.892188] can: raw protocol
[    1.895230] can: broadcast manager protocol
[    1.899401] can: netlink gateway - max_hops=1
[    1.904445] Bluetooth: RFCOMM TTY layer initialized
[    1.909385] Bluetooth: RFCOMM socket layer initialized
[    1.914653] Bluetooth: RFCOMM ver 1.11
[    1.918398] Bluetooth: BNEP (Ethernet Emulation) ver 1.3
[    1.923695] Bluetooth: BNEP filters: protocol multicast
[    1.929012] Bluetooth: BNEP socket layer initialized
[    1.933973] Bluetooth: HIDP (Human Interface Emulation) ver 1.2
[    1.939891] Bluetooth: HIDP socket layer initialized
[    1.944942] l2tp_core: L2TP core driver, V2.0
[    1.949289] l2tp_ppp: PPPoL2TP kernel driver, V2.0
[    1.954112] l2tp_netlink: L2TP netlink interface
[    1.959472] mmc0: host does not support reading read-only switch, assuming write-enable
[    1.968051] Loading compiled-in X.509 certificates
[    1.973632] mmc0: new high speed SDHC card at address b368
[    1.981273] mmcblk0: mmc0:b368 SDC   15.0 GiB
[    2.001043]  mmcblk0: p1 p2 p3 p4 < p5 p6 p7 >
[    2.036909] input: gpio-keys as /devices/platform/gpio-keys/input/input0
[    2.045930] ALSA device list:
[    2.048875]   No soundcards found.
[    2.060553] VFS: Mounted root (squashfs filesystem) readonly on device 179:3.
[    2.071754] devtmpfs: mounted
[    2.077592] Freeing unused kernel image (initmem) memory: 1024K
[    2.083843] Run /sbin/init as init process
[    2.263692] init: Console is alive
[    3.407957] kmodloader: loading kernel modules from /etc/modules-boot.d/*
[    3.443250] kmodloader: done loading kernel modules from /etc/modules-boot.d/*
[    3.461399] init: - preinit -
[    6.463808] random: crng init done
Press the [f] key and hit [enter] to enter failsafe mode
Press the [1], [2], [3] or [4] key and hit [enter] to select the debug level
[    9.994727] overlay-resize: Root device mmcblk0
[   10.215978] overlay-resize: Overlay partition /dev/mmcblk0p7 (disk /dev/mmcblk0)
[   10.410040] overlay-resize: EXT4 filesystem
[   11.022389] overlay-resize: Filesystem successfully checked (0)
[   13.227924] overlay-resize: Partition 4 resized to 31512542s
[   13.537775] overlay-resize: Partition 7 resized to 31512542s
[   13.544011] overlay-resize: Partition /dev/mmcblk0p7 end sector 1925119 -> 31512542
[   13.552027] overlay-resize: Resizing filesystem on partition /dev/mmcblk0p7...
[   69.969445] overlay-resize: Filesystem resized
[   70.230792] SWUPDATE: AT91Bootstrap version 4.0.5-gitAUTOINC+8fe4b67188-r0.tano3.2
[   70.431180] SWUPDATE: U-Boot version 2020.01-gitAUTOINC+af59b26c22-tano0.3
[   70.557007] SWUPDATE: Failed to extract the U-Boot startup version from '/dev/mmcblk1'
[   70.594558] SWUPDATE: Kernel version 6.1.74-tano-mchp-standard-g3f74b634cc-tano0.2.1.20.0.1
[   70.630842] SWUPDATE: Read-only filesystem version 2024-04-04-14-07-11-UTC
[   70.829672] mount_root: /dev/mmcblk0p1: p1, rw, start 1048576, size 8388608
[   70.843733] mount_root: /dev/mmcblk0p2: p2, rw, start 9437184, size 67108864
[   70.851464] mount_root: /dev/mmcblk0p3: p3, rw, start 76546048, size 402653184 [rootfs]
[   70.861218] mount_root: /dev/mmcblk0p4: p4, rw, start 480246784, size 1024
[   70.875190] mount_root: /dev/mmcblk0p5: p5, rw, start 480247808, size 67108864
[   70.884892] mount_root: /dev/mmcblk0p6: p6, rw, start 548405248, size 402653184
[   70.898681] mount_root: /dev/mmcblk0p7: p7, rw, start 952107008, size 15182315008 [overlay]
[   70.907415] mount_root: root filesystem on the /dev/mmcblk0p3 partition of /dev/mmcblk0 (rw) device
[   70.942337] mount_root: founded suitable overlay partition /dev/mmcblk0p7
[   70.955953] mount_root: loading kmods from internal overlay
[   71.325951] kmodloader: loading kernel modules from //etc/modules-boot.d/*
[   71.338064] kmodloader: done loading kernel modules from //etc/modules-boot.d/*
[   71.578085] EXT4-fs (mmcblk0p7): mounted filesystem with ordered data mode. Quota mode: disabled.
[   76.819254] block: attempting to load /tmp/ext4_cfg/upper/etc/config/fstab
[   76.827972] block: unable to load configuration (fstab: Entry not found)
[   76.834996] block: attempting to load /tmp/ext4_cfg/etc/config/fstab
[   76.841677] block: unable to load configuration (fstab: Entry not found)
[   76.848728] block: attempting to load /etc/config/fstab
[   76.869892] block: extroot: not configured
[   78.620736] EXT4-fs (mmcblk0p7): unmounting filesystem.
[   78.737607] EXT4-fs (mmcblk0p7): mounted filesystem with ordered data mode. Quota mode: disabled.
[   78.861991] block: attempting to load /tmp/ext4_cfg/upper/etc/config/fstab
[   78.869416] block: unable to load configuration (fstab: Entry not found)
[   78.876475] block: attempting to load /tmp/ext4_cfg/etc/config/fstab
[   78.883159] block: unable to load configuration (fstab: Entry not found)
[   78.890217] block: attempting to load /etc/config/fstab
[   78.896641] block: extroot: not configured
[   78.903943] mount_root: overlay filesystem has not been fully initialized yet
[   78.918315] mount_root: switching to ext4 overlay
[   79.697882] Root filesystem mounted
[   82.290514] urandom-seed: Seed file not found (/etc/urandom.seed)
[   82.387487] procd: - ubus -
[   82.571852] procd: - init -
Please press Enter to activate this console.
[   86.602743] kmodloader: loading kernel modules from /etc/modules.d/*
[   86.697219] tun: Universal TUN/TAP device driver, 1.6
[   86.905914] atmel_usba_udc 500000.gadget: MMIO registers at [mem 0xf8030000-0xf8033fff] mapped at 430665c2
[   86.915803] atmel_usba_udc 500000.gadget: FIFO at [mem 0x00500000-0x005fffff] mapped at dfae73ea
[   86.951836] usbcore: registered new interface driver cdc_wdm
[   87.035008] cfg80211: Loading compiled-in X.509 certificates for regulatory database
[   87.338620] cfg80211: Loaded X.509 cert 'sforshee: 00b28ddf47aef9cea7'
[   87.349513] cfg80211: Loaded X.509 cert 'wens: 61c038651aabdcf94bd0ac7ff06c7248db18c600'
[   87.358031] platform regulatory.0: Direct firmware load for regulatory.db failed with error -2
[   87.366733] cfg80211: failed to load regulatory.db
[   87.389515] cryptodev: loading out-of-tree module taints kernel.
[   87.416378] cryptodev: driver 1.12 loaded.
[   87.427265] gre: GRE over IPv4 demultiplexor driver
[   87.627339] PPTP driver version 0.8.5
[   87.769098] xt_time: kernel timezone is +0300
[   87.787977] usbcore: registered new interface driver cdc_ether
[   87.809277] usbcore: registered new interface driver cdc_ncm
[   87.903284] usbcore: registered new interface driver qmi_wwan
[   87.997503] usbcore: registered new interface driver cdc_mbim
[   88.023428] kmodloader: done loading kernel modules from /etc/modules.d/*
[  102.196520] Cgroup memory moving (move_charge_at_immigrate) is deprecated. Please report your usecase to linux-mm@kvack.org if you depend on this functionality.
[  106.436640] udevd[1411]: starting version 3.2.10
[  106.689788] udevd[1411]: starting eudev-3.2.10
[  155.578279] macb f0028000.ethernet eth0: PHY [f0028000.ethernet-ffffffff:07] driver [Micrel KSZ9031 Gigabit PHY] (irq=POLL)
[  155.589533] macb f0028000.ethernet eth0: configuring for phy/rgmii-rxid link mode
[  155.651589] br-lan: port 1(eth0) entered blocking state
[  155.656907] br-lan: port 1(eth0) entered disabled state
[  155.662656] device eth0 entered promiscuous mode
[  155.966660] macb f802c000.ethernet eth1: PHY [f802c000.ethernet-ffffffff:01] driver [Micrel KSZ8081 or KSZ8091] (irq=POLL)
[  155.977825] macb f802c000.ethernet eth1: configuring for phy/rmii link mode
[  158.806275] macb f0028000.ethernet eth0: Link is Up - 1Gbps/Full - flow control off
[  158.824573] br-lan: port 1(eth0) entered blocking state
[  158.829803] br-lan: port 1(eth0) entered forwarding state
[  158.879923] IPv6: ADDRCONF(NETDEV_CHANGE): br-lan: link becomes ready
[  192.095264] using random self ethernet address
[  192.099705] using random host ethernet address
[  192.105833] usb0: HOST MAC ca:a5:be:b9:82:ea
[  192.110100] usb0: MAC 3e:e5:81:4b:61:0f
[  192.114085] using random self ethernet address
[  192.118497] using random host ethernet address
[  192.123077] g_ether gadget.0: Ethernet Gadget, version: Memorial Day 2008
[  192.129934] g_ether gadget.0: g_ether ready
[  192.304090] IPv6: ADDRCONF(NETDEV_CHANGE): usb0: link becomes ready


tanowrt login: root
Password:
 _______            __          __   _
|__   __|           \ \        / /  | |    Embedded Linux Distribution
   | | __ _ _ __   __\ \  /\  / / __| |_   by Tano Systems
   | |/ _` | '_ \ / _ \ \/  \/ / '__| __|
   | | (_| | | | | (_) \  /\  /| |  | |_   (c) 2018-2024 Tano Systems LLC
   |_|\__,_|_| |_|\___/ \/  \/ |_|   \__|  https://tano-systems.com

   Board:    Atmel SAMA5D3 Xplained (SD)
   Release:  Kirkstone
   Revision: 981efb8a6cdf08c0071d86ae3ad626ab608bac4b
             2024-04-04 14:07:11 UTC

[root@tanowrt ~]#
