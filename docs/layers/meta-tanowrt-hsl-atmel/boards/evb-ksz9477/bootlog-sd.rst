.. SPDX-License-Identifier: MIT

Microchip EVB-KSZ9477 Booting from External SD Card Logs
========================================================

AT91Bootstrap
-------------

.. tabs::

   .. tab:: 4.0.5

      .. literalinclude :: logs/bootlog-sd-at91bootstrap-4.0.5.txt
         :language: text

U-Boot
------

.. tabs::

   .. tab:: 2020.01

      .. literalinclude :: logs/bootlog-sd-u-boot-2020.01.txt
         :language: text

Linux Kernel (OS)
-----------------

.. note::

   This is the first power-on logs after writing an image to the SD card,
   so there is a step of automatically resizong (expanding) the overlay
   partition to the entire SD card.

.. tabs::

   .. tab:: 6.1.74

      .. literalinclude :: logs/bootlog-sd-kernel-6.1.74.txt
         :language: text
