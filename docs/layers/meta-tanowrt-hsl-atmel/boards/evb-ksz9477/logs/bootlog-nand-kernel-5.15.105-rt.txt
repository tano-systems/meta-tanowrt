[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Linux version 5.15.105-rt61-tano-mchp-rt-g22ed83424b-tano0.2.2.20.0.2 (oe-user@oe-host) (arm-oe-linux-gnueabi-gcc (GCC) 11.4.0, GNU ld (GNU Binutils) 2.38.20220708) #1 PREEMPT_RT Fri Apr 14 11:46:12 UTC 2023
[    0.000000] CPU: ARMv7 Processor [410fc051] revision 1 (ARMv7), cr=10c53c7d
[    0.000000] CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
[    0.000000] OF: fdt: Machine model: Microchip EVB-KSZ9477
[    0.000000] Memory policy: Data cache writeback
[    0.000000] cma: Reserved 16 MiB at 0x2f000000
[    0.000000] Zone ranges:
[    0.000000]   Normal   [mem 0x0000000020000000-0x000000002fffffff]
[    0.000000] Movable zone start for each node
[    0.000000] Early memory node ranges
[    0.000000]   node   0: [mem 0x0000000020000000-0x000000002fffffff]
[    0.000000] Initmem setup node 0 [mem 0x0000000020000000-0x000000002fffffff]
[    0.000000] CPU: All CPU(s) started in SVC mode.
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 64960
[    0.000000] Kernel command line: console=ttyS0,115200n8  ubi.mtd=system_a,2048 ubi.mtd=rootfs_data,2048 ubi.block=0,rootfs root=/dev/ubiblock0_1 ro rootfstype=squashfs rootwait rootfs_partition=system_a rootfs_volume=rootfs earlyprintk panic=15
[    0.000000] Unknown kernel command line parameters "earlyprintk rootfs_partition=system_a rootfs_volume=rootfs", will be passed to user space.
[    0.000000] Dentry cache hash table entries: 32768 (order: 5, 131072 bytes, linear)
[    0.000000] Inode-cache hash table entries: 16384 (order: 4, 65536 bytes, linear)
[    0.000000] mem auto-init: stack:off, heap alloc:off, heap free:off
[    0.000000] Memory: 229068K/262144K available (9216K kernel code, 473K rwdata, 2212K rodata, 1024K init, 165K bss, 16692K reserved, 16384K cma-reserved)
[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=1, Nodes=1
[    0.000000] rcu: Preemptible hierarchical RCU implementation.
[    0.000000] rcu:     RCU event tracing is enabled.
[    0.000000] rcu:     RCU priority boosting: priority 1 delay 500 ms.
[    0.000000] rcu:     RCU_SOFTIRQ processing moved to rcuc kthreads.
[    0.000000]  No expedited grace period (rcu_normal_after_boot).
[    0.000000]  Trampoline variant of Tasks RCU enabled.
[    0.000000] rcu: RCU calculated value of scheduler-enlistment delay is 10 jiffies.
[    0.000000] NR_IRQS: 16, nr_irqs: 16, preallocated irqs: 16
[    0.000000] clocksource: pit: mask: 0xfffffff max_cycles: 0xfffffff, max_idle_ns: 14479245754 ns
[    0.000000] Console: colour dummy device 80x30
[    0.000000] sched_clock: 32 bits at 100 Hz, resolution 10000000ns, wraps every 21474836475000000ns
[    0.000000] Calibrating delay loop... 351.43 BogoMIPS (lpj=1757184)
[    0.060000] pid_max: default: 32768 minimum: 301
[    0.060000] Mount-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
[    0.060000] Mountpoint-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
[    0.060000] CPU: Testing write buffer coherency: ok
[    0.060000] Setting up static identity map for 0x20100000 - 0x20100060
[    0.060000] rcu: Hierarchical SRCU implementation.
[    0.060000] devtmpfs: initialized
[    0.090000] VFP support v0.3: implementor 41 architecture 2 part 30 variant 5 rev 1
[    0.090000] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604462750000 ns
[    0.090000] futex hash table entries: 256 (order: 0, 6144 bytes, linear)
[    0.100000] pinctrl core: initialized pinctrl subsystem
[    0.100000] NET: Registered PF_NETLINK/PF_ROUTE protocol family
[    0.100000] DMA: preallocated 256 KiB pool for atomic coherent allocations
[    0.110000] cpuidle: using governor menu
[    0.160000] AT91: PM: standby: standby, suspend: ulp0
[    0.160000] gpio-at91 fffff200.gpio: at address (ptrval)
[    0.170000] gpio-at91 fffff400.gpio: at address (ptrval)
[    0.170000] gpio-at91 fffff600.gpio: at address (ptrval)
[    0.170000] gpio-at91 fffff800.gpio: at address (ptrval)
[    0.170000] gpio-at91 fffffa00.gpio: at address (ptrval)
[    0.170000] pinctrl-at91 ahb:apb:pinctrl@fffff200: initialized AT91 pinctrl driver
[    0.230000] at_hdmac ffffe600.dma-controller: Atmel AHB DMA Controller ( cpy set slave ), 8 channels
[    0.230000] at_hdmac ffffe800.dma-controller: Atmel AHB DMA Controller ( cpy set slave ), 8 channels
[    0.240000] AT91: Detected SoC family: sama5d3
[    0.240000] AT91: Detected SoC: sama5d36, revision 2
[    0.240000] SCSI subsystem initialized
[    0.240000] usbcore: registered new interface driver usbfs
[    0.240000] usbcore: registered new interface driver hub
[    0.240000] usbcore: registered new device driver usb
[    0.240000] at91_i2c f0014000.i2c: using dma0chan0 (tx) and dma0chan1 (rx) for DMA transfers
[    0.240000] i2c i2c-0: using pinctrl states for GPIO recovery
[    0.240000] i2c i2c-0: using generic GPIOs for recovery
[    0.240000] at91_i2c f0014000.i2c: AT91 i2c bus driver (hw version: 0x402).
[    0.250000] at91_i2c f0018000.i2c: using dma0chan2 (tx) and dma0chan3 (rx) for DMA transfers
[    0.250000] i2c i2c-1: using pinctrl states for GPIO recovery
[    0.250000] i2c i2c-1: using generic GPIOs for recovery
[    0.250000] at91_i2c f0018000.i2c: AT91 i2c bus driver (hw version: 0x402).
[    0.250000] at91_i2c f801c000.i2c: can't get DMA channel, continue without DMA support
[    0.250000] i2c i2c-2: using pinctrl states for GPIO recovery
[    0.250000] i2c i2c-2: using generic GPIOs for recovery
[    0.250000] at91_i2c f801c000.i2c: AT91 i2c bus driver (hw version: 0x402).
[    0.250000] mc: Linux media interface: v0.10
[    0.250000] videodev: Linux video capture interface: v2.00
[    0.250000] pps_core: LinuxPPS API ver. 1 registered
[    0.250000] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    0.250000] PTP clock support registered
[    0.250000] Advanced Linux Sound Architecture Driver Initialized.
[    0.250000] Bluetooth: Core ver 2.22
[    0.250000] NET: Registered PF_BLUETOOTH protocol family
[    0.250000] Bluetooth: HCI device and connection manager initialized
[    0.250000] Bluetooth: HCI socket layer initialized
[    0.250000] Bluetooth: L2CAP socket layer initialized
[    0.250000] Bluetooth: SCO socket layer initialized
[    0.260000] clocksource: Switched to clocksource pit
[    0.290000] NET: Registered PF_INET protocol family
[    0.290000] IP idents hash table entries: 4096 (order: 3, 32768 bytes, linear)
[    0.300000] tcp_listen_portaddr_hash hash table entries: 256 (order: 0, 5120 bytes, linear)
[    0.300000] Table-perturb hash table entries: 65536 (order: 6, 262144 bytes, linear)
[    0.300000] TCP established hash table entries: 2048 (order: 1, 8192 bytes, linear)
[    0.300000] TCP bind hash table entries: 2048 (order: 3, 32768 bytes, linear)
[    0.300000] TCP: Hash tables configured (established 2048 bind 2048)
[    0.300000] UDP hash table entries: 256 (order: 1, 12288 bytes, linear)
[    0.300000] UDP-Lite hash table entries: 256 (order: 1, 12288 bytes, linear)
[    0.300000] NET: Registered PF_UNIX/PF_LOCAL protocol family
[    0.300000] RPC: Registered named UNIX socket transport module.
[    0.300000] RPC: Registered udp transport module.
[    0.300000] RPC: Registered tcp transport module.
[    0.300000] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    0.300000] Initialise system trusted keyrings
[    0.300000] workingset: timestamp_bits=14 max_order=16 bucket_order=2
[    0.330000] squashfs: version 4.0 (2009/01/31) Phillip Lougher
[    0.540000] Key type asymmetric registered
[    0.540000] Asymmetric key parser 'x509' registered
[    0.540000] io scheduler mq-deadline registered
[    0.540000] io scheduler kyber registered
[    0.570000] brd: module loaded
[    0.590000] loop: module loaded
[    0.590000] atmel_usart_serial.0.auto: ttyS1 at MMIO 0xf001c000 (irq = 23, base_baud = 4125000) is a ATMEL_SERIAL
[    0.600000] atmel_usart_serial.1.auto: ttyS2 at MMIO 0xf0020000 (irq = 24, base_baud = 4125000) is a ATMEL_SERIAL
[    0.600000] atmel_usart_serial.2.auto: ttyS5 at MMIO 0xf0024000 (irq = 25, base_baud = 4125000) is a ATMEL_SERIAL
[    0.600000] atmel_usart_serial.3.auto: ttyS0 at MMIO 0xffffee00 (irq = 38, base_baud = 8250000) is a ATMEL_SERIAL
[    0.600000] printk: console [ttyS0] enabled
[    0.610000] atmel_spi f0004000.spi: Using dma0chan4 (tx) and dma0chan5 (rx) for DMA transfers
[    0.610000] atmel_spi f0004000.spi: Atmel SPI Controller version 0x213 at 0xf0004000 (irq 19)
[    0.610000] atmel_spi f8008000.spi: Using dma1chan0 (tx) and dma1chan1 (rx) for DMA transfers
[    0.610000] ksz9477@0 enforce active low on chipselect handle
[    0.620000] atmel_spi f8008000.spi: Atmel SPI Controller version 0x213 at 0xf8008000 (irq 28)
[    0.620000] CAN device driver interface
[    0.620000] ksz9477-switch spi1.0: Microchip KSZ switch driver version 0.9.0
[    0.620000] ksz9477-switch spi1.0: Microchip KSZ9477 Switch (7 ports)
[    0.620000] ksz9477-switch spi1.0: Failed to register DSA switch (-517)
[    1.280000] macb f0028000.ethernet eth0: Cadence GEM rev 0x00020119 at 0xf0028000 irq 46 (00:10:a1:98:97:01)
[    1.280000] PPP generic driver version 2.4.2
[    1.280000] PPP MPPE Compression module registered
[    1.280000] NET: Registered PF_PPPOX protocol family
[    1.280000] ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
[    1.280000] ehci-atmel: EHCI Atmel driver
[    1.280000] atmel-ehci 700000.ehci: EHCI Host Controller
[    1.280000] atmel-ehci 700000.ehci: new USB bus registered, assigned bus number 1
[    1.280000] atmel-ehci 700000.ehci: irq 49, io mem 0x00700000
[    1.310000] atmel-ehci 700000.ehci: USB 2.0 started, EHCI 1.00
[    1.310000] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002, bcdDevice= 5.15
[    1.310000] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.310000] usb usb1: Product: EHCI Host Controller
[    1.310000] usb usb1: Manufacturer: Linux 5.15.105-rt61-tano-mchp-rt-g22ed83424b-tano0.2.2.20.0.2 ehci_hcd
[    1.310000] usb usb1: SerialNumber: 700000.ehci
[    1.310000] hub 1-0:1.0: USB hub found
[    1.310000] hub 1-0:1.0: 3 ports detected
[    1.310000] ohci_hcd: USB 1.1 'Open' Host Controller (OHCI) Driver
[    1.310000] ohci-atmel: OHCI Atmel driver
[    1.310000] at91_ohci 600000.ohci: USB Host Controller
[    1.310000] at91_ohci 600000.ohci: new USB bus registered, assigned bus number 2
[    1.310000] at91_ohci 600000.ohci: irq 49, io mem 0x00600000
[    1.380000] usb usb2: New USB device found, idVendor=1d6b, idProduct=0001, bcdDevice= 5.15
[    1.380000] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.380000] usb usb2: Product: USB Host Controller
[    1.380000] usb usb2: Manufacturer: Linux 5.15.105-rt61-tano-mchp-rt-g22ed83424b-tano0.2.2.20.0.2 ohci_hcd
[    1.380000] usb usb2: SerialNumber: at91
[    1.380000] hub 2-0:1.0: USB hub found
[    1.380000] hub 2-0:1.0: 3 ports detected
[    1.390000] usbcore: registered new interface driver cdc_acm
[    1.390000] cdc_acm: USB Abstract Control Model driver for USB modems and ISDN adapters
[    1.390000] usbcore: registered new interface driver usb-storage
[    1.390000] usbcore: registered new interface driver usbserial_generic
[    1.390000] usbserial: USB Serial support registered for generic
[    1.390000] usbcore: registered new interface driver ftdi_sio
[    1.390000] usbserial: USB Serial support registered for FTDI USB Serial Device
[    1.390000] at91_rtc fffffeb0.rtc: registered as rtc0
[    1.390000] at91_rtc fffffeb0.rtc: setting system clock to 2024-04-12T12:05:43 UTC (1712923543)
[    1.390000] at91_rtc fffffeb0.rtc: AT91 Real Time Clock driver.
[    1.390000] i2c_dev: i2c /dev entries driver
[    1.390000] at91-reset fffffe00.rstc: Starting after software reset
[    1.400000] at91_wdt fffffe40.watchdog: watchdog already configured differently (mr = 1fff2eff expecting 1fff2fff)
[    1.400000] at91sam9_wdt: enabled (heartbeat=15 sec, nowayout=0)
[    1.400000] Bluetooth: HCI UART driver ver 2.3
[    1.400000] Bluetooth: HCI UART protocol H4 registered
[    1.400000] Bluetooth: HCI UART protocol Three-wire (H5) registered
[    1.400000] usbcore: registered new interface driver btusb
[    1.400000] sdhci: Secure Digital Host Controller Interface driver
[    1.400000] sdhci: Copyright(c) Pierre Ossman
[    1.400000] sdhci-pltfm: SDHCI platform and OF driver helper
[    1.400000] atmel_aes f8038000.crypto: version: 0x135
[    1.410000] atmel_mci f0000000.mmc: version: 0x505
[    1.410000] atmel_mci f0000000.mmc: using dma0chan6 for DMA transfers
[    1.410000] atmel_mci f8000000.mmc: version: 0x505
[    1.410000] atmel_mci f8000000.mmc: using dma1chan4 for DMA transfers
[    1.410000] atmel_aes f8038000.crypto: Atmel AES - Using dma1chan2, dma1chan3 for DMA transfers
[    1.420000] atmel_sha f8034000.crypto: version: 0x410
[    1.420000] atmel_sha f8034000.crypto: using dma1chan5 for DMA transfers
[    1.420000] atmel_sha f8034000.crypto: Atmel SHA1/SHA256/SHA224/SHA384/SHA512
[    1.420000] atmel_tdes f803c000.crypto: version: 0x701
[    1.420000] atmel_tdes f803c000.crypto: using dma1chan6, dma1chan7 for DMA transfers
[    1.430000] atmel_tdes f803c000.crypto: Atmel DES/TDES
[    1.430000] usbcore: registered new interface driver usbhid
[    1.430000] usbhid: USB HID core driver
[    1.430000] nand: device found, Manufacturer ID: 0x2c, Chip ID: 0xda
[    1.430000] nand: Micron MT29F2G08ABAEAWP
[    1.430000] nand: 256 MiB, SLC, erase size: 128 KiB, page size: 2048, OOB size: 64
[    1.430000] Bad block table found at page 131008, version 0x01
[    1.440000] Bad block table found at page 130944, version 0x01
[    1.440000] 7 fixed-partitions partitions found on MTD device atmel_nand
[    1.440000] Creating 7 MTD partitions on "atmel_nand":
[    1.440000] 0x000000000000-0x000000040000 : "at91bootstrap"
[    1.440000] 0x000000040000-0x000000180000 : "uboot"
[    1.450000] 0x000000180000-0x0000001c0000 : "startup"
[    1.450000] 0x0000001c0000-0x000000200000 : "ubootenv"
[    1.460000] atmel_mci f8000000.mmc: Atmel MCI controller at 0xf8000000 irq 27, 1 slots
[    1.460000] atmel_mci f0000000.mmc: Atmel MCI controller at 0xf0000000 irq 18, 1 slots
[    1.460000] 0x000000200000-0x000006a00000 : "system_a"
[    1.470000] 0x000006a00000-0x00000d200000 : "system_b"
[    1.470000] 0x00000d200000-0x000010000000 : "rootfs_data"
[    1.480000] iio iio:device0: Resolution used: 12 bits
[    1.480000] iio iio:device0: ADC Touch screen is disabled.
[    1.490000] u32 classifier
[    1.490000]     input device check on
[    1.490000]     Actions configured
[    1.490000] NET: Registered PF_INET6 protocol family
[    1.490000] Segment Routing with IPv6
[    1.490000] In-situ OAM (IOAM) with IPv6
[    1.490000] sit: IPv6, IPv4 and MPLS over IPv4 tunneling driver
[    1.500000] NET: Registered PF_PACKET protocol family
[    1.500000] Bridge firewalling registered
[    1.500000] can: controller area network core
[    1.500000] NET: Registered PF_CAN protocol family
[    1.500000] can: raw protocol
[    1.500000] can: broadcast manager protocol
[    1.500000] can: netlink gateway - max_hops=1
[    1.500000] Bluetooth: RFCOMM TTY layer initialized
[    1.500000] Bluetooth: RFCOMM socket layer initialized
[    1.500000] Bluetooth: RFCOMM ver 1.11
[    1.500000] Bluetooth: BNEP (Ethernet Emulation) ver 1.3
[    1.500000] Bluetooth: BNEP filters: protocol multicast
[    1.500000] Bluetooth: BNEP socket layer initialized
[    1.500000] Bluetooth: HIDP (Human Interface Emulation) ver 1.2
[    1.500000] Bluetooth: HIDP socket layer initialized
[    1.500000] l2tp_core: L2TP core driver, V2.0
[    1.500000] l2tp_ppp: PPPoL2TP kernel driver, V2.0
[    1.500000] l2tp_netlink: L2TP netlink interface
[    1.500000] printk: console [ttyS0]: printing thread started
[    1.500000] Loading compiled-in X.509 certificates
[    1.540000] ksz9477-switch spi1.0: Microchip KSZ switch driver version 0.9.0
[    1.610000] ksz9477-switch spi1.0: Microchip KSZ9477 Switch (7 ports)
[    1.610000] ksz9477-switch spi1.0: Use rgmii-txid
[    1.620000] ksz9477-switch spi1.0: Port 5: Enabled tail tagging
[    3.090000] ksz9477-switch spi1.0: Enabled jumbo frames support
[    3.100000] ksz9477-switch spi1.0: The switch has been successfully started
[    3.140000] ksz9477-switch spi1.0 sw1p1 (uninitialized): PHY [dsa-0.0:00] driver [Microchip KSZ9477 Switch] (irq=POLL)
[    3.140000] ksz9477-switch spi1.0 sw1p2 (uninitialized): PHY [dsa-0.0:01] driver [Microchip KSZ9477 Switch] (irq=POLL)
[    3.140000] ksz9477-switch spi1.0 sw1p3 (uninitialized): PHY [dsa-0.0:02] driver [Microchip KSZ9477 Switch] (irq=POLL)
[    3.180000] ksz9477-switch spi1.0 sw1p4 (uninitialized): PHY [dsa-0.0:03] driver [Microchip KSZ9477 Switch] (irq=POLL)
[    3.190000] ksz9477-switch spi1.0 sw1p5 (uninitialized): PHY [dsa-0.0:04] driver [Microchip KSZ9477 Switch] (irq=POLL)
[    3.190000] ksz9477-switch spi1.0: Using legacy PHYLIB callbacks. Please migrate to PHYLINK!
[    3.190000] DSA: tree 0 setup
[    3.190000] ksz9477-switch spi1.0: User ports mask is 0x5f
[    3.190000] ksz9477-switch spi1.0: CPU port MAC: 00:10:a1:98:97:01
[    3.190000] ksz9477-switch spi1.0: Port 0 [sw1p1] MAC: 00:10:a1:98:97:02
[    3.210000] ksz9477-switch spi1.0: Port 1 [sw1p2] MAC: 00:10:a1:98:97:03
[    3.210000] ksz9477-switch spi1.0: Port 2 [sw1p3] MAC: 00:10:a1:98:97:04
[    3.210000] ksz9477-switch spi1.0: Port 3 [sw1p4] MAC: 00:10:a1:98:97:05
[    3.210000] ksz9477-switch spi1.0: Port 4 [sw1p5] MAC: 00:10:a1:98:97:06
[    3.210000] ksz9477-switch spi1.0: Port 6 [sw1p6] MAC: 00:10:a1:98:97:07
[    3.210000] ksz9477-switch spi1.0: Port mask: 0x7f, CPU mask: 0x20, DSA mask: 0x0, user mask: 0x5f
[    3.210000] ksz9477-switch spi1.0: Port 0 [USER]: sw1p1
[    3.210000] ksz9477-switch spi1.0: Port 1 [USER]: sw1p2
[    3.210000] ksz9477-switch spi1.0: Port 2 [USER]: sw1p3
[    3.210000] ksz9477-switch spi1.0: Port 3 [USER]: sw1p4
[    3.210000] ksz9477-switch spi1.0: Port 4 [USER]: sw1p5
[    3.210000] ksz9477-switch spi1.0: Port 5 [CPU]: eth0
[    3.210000] ksz9477-switch spi1.0: Port 6 [USER]: sw1p6
[    3.210000] ksz9477-switch spi1.0: DSA switch registered
[    3.210000] ksz9477-switch spi1.0: Created sysfs entries
[    3.210000] ubi0: default fastmap pool size: 40
[    3.210000] ubi0: default fastmap WL pool size: 20
[    3.210000] ubi0: attaching mtd4
[    4.010000] ubi0: scanning is finished
[    4.030000] ubi0: attached mtd4 (name "system_a", size 104 MiB)
[    4.030000] ubi0: PEB size: 131072 bytes (128 KiB), LEB size: 126976 bytes
[    4.030000] ubi0: min./max. I/O unit sizes: 2048/2048, sub-page size 2048
[    4.030000] ubi0: VID header offset: 2048 (aligned 2048), data offset: 4096
[    4.030000] ubi0: good PEBs: 832, bad PEBs: 0, corrupted PEBs: 0
[    4.030000] ubi0: user volume: 2, internal volumes: 1, max. volumes count: 128
[    4.030000] ubi0: max/mean erase counter: 32/25, WL threshold: 4096, image sequence number: 345045444
[    4.030000] ubi0: available PEBs: 538, total reserved PEBs: 294, PEBs reserved for bad PEB handling: 40
[    4.030000] ubi1: default fastmap pool size: 15
[    4.030000] ubi1: default fastmap WL pool size: 7
[    4.030000] ubi1: attaching mtd6
[    4.030000] ubi0: background thread "ubi_bgt0d" started, PID 128
[    4.420000] ubi1: scanning is finished
[    4.430000] ubi1: attached mtd6 (name "rootfs_data", size 46 MiB)
[    4.430000] ubi1: PEB size: 131072 bytes (128 KiB), LEB size: 126976 bytes
[    4.430000] ubi1: min./max. I/O unit sizes: 2048/2048, sub-page size 2048
[    4.430000] ubi1: VID header offset: 2048 (aligned 2048), data offset: 4096
[    4.430000] ubi1: good PEBs: 364, bad PEBs: 4, corrupted PEBs: 0
[    4.430000] ubi1: user volume: 1, internal volumes: 1, max. volumes count: 128
[    4.430000] ubi1: max/mean erase counter: 51/26, WL threshold: 4096, image sequence number: 814988759
[    4.430000] ubi1: available PEBs: 0, total reserved PEBs: 364, PEBs reserved for bad PEB handling: 36
[    4.450000] ubi1: background thread "ubi_bgt1d" started, PID 129
[    4.460000] block ubiblock0_1: created from ubi0:1(rootfs)
[    4.460000] ALSA device list:
[    4.460000]   No soundcards found.
[    4.470000] VFS: Mounted root (squashfs filesystem) readonly on device 254:0.
[    4.470000] devtmpfs: mounted
[    4.490000] Freeing unused kernel image (initmem) memory: 1024K
[    4.520000] Run /sbin/init as init process
[    4.910000] init: Console is alive
[    4.910000] init: - watchdog -
[    6.100000] random: crng init done
[    6.480000] kmodloader: loading kernel modules from /etc/modules-boot.d/*
[    6.520000] kmodloader: done loading kernel modules from /etc/modules-boot.d/*
[    6.530000] init: - preinit -
Press the [f] key and hit [enter] to enter failsafe mode
Press the [1], [2], [3] or [4] key and hit [enter] to select the debug level
[   11.140000] SWUPDATE: AT91Bootstrap version 4.0.8-gitAUTOINC+c331a16179-r0.tano3.3
[   11.610000] SWUPDATE: U-Boot version 2023.07.02-gitAUTOINC+842d9d53a1-tano0.3
[   11.730000] SWUPDATE: U-Boot startup version 1.0.0-tano1.atmel1
[   11.770000] SWUPDATE: Kernel version 5.15.105-rt61-tano-mchp-rt-g22ed83424b-tano0.2.2.20.0.2
[   11.810000] SWUPDATE: Read-only filesystem version 2024-04-12-08-40-02-UTC
[   12.240000] mount_root: loading kmods from internal overlay
[   12.640000] kmodloader: loading kernel modules from //etc/modules-boot.d/*
[   12.660000] kmodloader: done loading kernel modules from //etc/modules-boot.d/*
[   12.830000] UBIFS (ubi1:0): Mounting in unauthenticated mode
[   12.830000] UBIFS (ubi1:0): background thread "ubifs_bgt1_0" started, PID 217
[   12.900000] UBIFS (ubi1:0): recovery needed
[   13.030000] UBIFS (ubi1:0): recovery completed
[   13.030000] UBIFS (ubi1:0): UBIFS: mounted UBI device 1, volume 0, name "rootfs_data"
[   13.030000] UBIFS (ubi1:0): LEB size: 126976 bytes (124 KiB), min./max. I/O unit sizes: 2048 bytes/2048 bytes
[   13.030000] UBIFS (ubi1:0): FS size: 39616512 bytes (37 MiB, 312 LEBs), max 322 LEBs, journal size 2031616 bytes (1 MiB, 16 LEBs)
[   13.030000] UBIFS (ubi1:0): reserved for root: 1871185 bytes (1827 KiB)
[   13.030000] UBIFS (ubi1:0): media format: w5/r0 (latest is w5/r0), UUID C8AD6E3E-8929-45DD-9034-4515EF9942B0, small LPT model
[   13.030000] block: attempting to load /tmp/ubifs_cfg/upper/etc/config/fstab
[   13.060000] block: extroot: not configured
[   13.090000] UBIFS (ubi1:0): un-mount UBI device 1
[   13.090000] UBIFS (ubi1:0): background thread "ubifs_bgt1_0" stops
[   13.120000] UBIFS (ubi1:0): Mounting in unauthenticated mode
[   13.120000] UBIFS (ubi1:0): background thread "ubifs_bgt1_0" started, PID 218
[   13.270000] UBIFS (ubi1:0): UBIFS: mounted UBI device 1, volume 0, name "rootfs_data"
[   13.270000] UBIFS (ubi1:0): LEB size: 126976 bytes (124 KiB), min./max. I/O unit sizes: 2048 bytes/2048 bytes
[   13.270000] UBIFS (ubi1:0): FS size: 39616512 bytes (37 MiB, 312 LEBs), max 322 LEBs, journal size 2031616 bytes (1 MiB, 16 LEBs)
[   13.270000] UBIFS (ubi1:0): reserved for root: 1871185 bytes (1827 KiB)
[   13.270000] UBIFS (ubi1:0): media format: w5/r0 (latest is w5/r0), UUID C8AD6E3E-8929-45DD-9034-4515EF9942B0, small LPT model
[   13.410000] block: attempting to load /tmp/ubifs_cfg/upper/etc/config/fstab
[   13.420000] block: extroot: not configured
[   13.450000] mount_root: switching to ubifs overlay
[   13.650000] Root filesystem mounted
[   16.390000] urandom-seed: Seeding with /etc/urandom.seed
[   16.500000] procd: - watchdog -
[   16.520000] procd: - ubus -
[   16.710000] procd: - init -
Please press Enter to activate this console.
[   21.160000] kmodloader: loading kernel modules from /etc/modules.d/*
[   21.210000] tun: Universal TUN/TAP device driver, 1.6
[   21.410000] atmel_usba_udc 500000.gadget: MMIO registers at [mem 0xf8030000-0xf8033fff] mapped at 79ebcc66
[   21.410000] atmel_usba_udc 500000.gadget: FIFO at [mem 0x00500000-0x005fffff] mapped at b9fc0435
[   21.460000] usbcore: registered new interface driver cdc_wdm
[   21.570000] cfg80211: Loading compiled-in X.509 certificates for regulatory database
[   21.890000] cfg80211: Loaded X.509 cert 'sforshee: 00b28ddf47aef9cea7'
[   21.890000] platform regulatory.0: Direct firmware load for regulatory.db failed with error -2
[   21.890000] cfg80211: failed to load regulatory.db
[   21.890000] cryptodev: loading out-of-tree module taints kernel.
[   21.930000] cryptodev: driver 1.12 loaded.
[   21.930000] gre: GRE over IPv4 demultiplexor driver
[   22.220000] PPTP driver version 0.8.5
[   22.380000] xt_time: kernel timezone is +0300
[   22.400000] usbcore: registered new interface driver cdc_ether
[   22.400000] usbcore: registered new interface driver cdc_ncm
[   22.500000] usbcore: registered new interface driver qmi_wwan
[   22.560000] usbcore: registered new interface driver cdc_mbim
[   22.570000] kmodloader: done loading kernel modules from /etc/modules.d/*
[   30.070000] Cgroup memory moving (move_charge_at_immigrate) is deprecated. Please report your usecase to linux-mm@kvack.org if you depend on this functionality.
[   35.460000] udevd[1160]: starting version 3.2.10
[   35.960000] udevd[1160]: starting eudev-3.2.10
[   73.560000] macb f0028000.ethernet eth0: configuring for fixed/rgmii link mode
[   73.560000] macb f0028000.ethernet eth0: Link is Up - 1Gbps/Full - flow control off
[   73.620000] device eth0 entered promiscuous mode
[   74.770000] ksz9477-switch spi1.0 sw1p1: configuring for phy/gmii link mode
[   74.790000] ksz9477-switch spi1.0 sw1p1: Link is Up - 1Gbps/Full - flow control off
[   74.790000] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
[   74.790000] br-lan: port 1(sw1p1) entered blocking state
[   74.790000] br-lan: port 1(sw1p1) entered disabled state
[   75.950000] device sw1p1 entered promiscuous mode
[   76.360000] br-lan: port 1(sw1p1) entered blocking state
[   78.000000] ksz9477-switch spi1.0 sw1p2: configuring for phy/gmii link mode
[   78.000000] br-lan: port 2(sw1p2) entered blocking state
[   78.000000] br-lan: port 2(sw1p2) entered disabled state
[   78.860000] device sw1p2 entered promiscuous mode
[   78.870000] br-lan: port 1(sw1p1) entered blocking state
[   80.390000] ksz9477-switch spi1.0 sw1p3: configuring for phy/gmii link mode
[   80.420000] br-lan: port 3(sw1p3) entered blocking state
[   80.420000] br-lan: port 3(sw1p3) entered disabled state
[   80.950000] device sw1p3 entered promiscuous mode
[   80.960000] br-lan: port 1(sw1p1) entered learning state
[   80.960000] br-lan: port 1(sw1p1) entered forwarding state
[   80.990000] IPv6: ADDRCONF(NETDEV_CHANGE): br-lan: link becomes ready
[   82.210000] ksz9477-switch spi1.0 sw1p4: configuring for phy/gmii link mode
[   82.240000] br-lan: port 4(sw1p4) entered blocking state
[   82.240000] br-lan: port 4(sw1p4) entered disabled state
[   82.690000] device sw1p4 entered promiscuous mode
[   83.110000] ksz9477-switch spi1.0 sw1p6: configuring for fixed/ link mode
[   83.110000] ksz9477-switch spi1.0 sw1p6: Link is Up - 1Gbps/Full - flow control off
[   83.320000] br-lan: port 5(sw1p6) entered blocking state
[   83.320000] br-lan: port 5(sw1p6) entered disabled state
[   84.280000] device sw1p6 entered promiscuous mode
[   84.280000] br-lan: port 5(sw1p6) entered blocking state
[   85.600000] ksz9477-switch spi1.0 sw1p5: configuring for phy/gmii link mode
[   85.630000] br-lan: port 5(sw1p6) entered blocking state
[   85.650000] br-lan: port 5(sw1p6) entered learning state
[   85.650000] br-lan: port 5(sw1p6) entered forwarding state
[  100.300000] using random self ethernet address
[  100.300000] using random host ethernet address
[  100.300000] usb0: HOST MAC fe:cb:8d:20:44:2f
[  100.300000] usb0: MAC ae:ba:b9:aa:b2:97
[  100.340000] using random self ethernet address
[  100.340000] using random host ethernet address
[  100.340000] g_ether gadget: Ethernet Gadget, version: Memorial Day 2008
[  100.340000] g_ether gadget: g_ether ready


tanowrt login: root
Password:
 _______            __          __   _
|__   __|           \ \        / /  | |    Embedded Linux Distribution
   | | __ _ _ __   __\ \  /\  / / __| |_   by Tano Systems
   | |/ _` | '_ \ / _ \ \/  \/ / '__| __|
   | | (_| | | | | (_) \  /\  /| |  | |_   (c) 2018-2024 Tano Systems LLC
   |_|\__,_|_| |_|\___/ \/  \/ |_|   \__|  https://tano-systems.com

   Board:    Microchip EVB-KSZ9477 (NAND)
   Release:  Kirkstone
   Revision: 30f32be34959042d70884ae8df562f41cd687826
             2024-04-12 08:40:02 UTC

[root@tanowrt ~]#
