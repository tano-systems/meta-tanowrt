.. SPDX-License-Identifier: MIT

Microchip EVB-KSZ9477 Booting from Internal NAND Flash Logs
===========================================================

AT91Bootstrap
-------------

.. tabs::

   .. tab:: 4.0.8

      .. literalinclude :: logs/bootlog-nand-at91bootstrap-4.0.8.txt
         :language: text

   .. tab:: 4.0.5

      .. literalinclude :: logs/bootlog-nand-at91bootstrap-4.0.5.txt
         :language: text

U-Boot
------

.. tabs::

   .. tab:: 2023.07

      .. literalinclude :: logs/bootlog-nand-u-boot-2023.07.txt
         :language: text

   .. tab:: 2020.01

      .. literalinclude :: logs/bootlog-nand-u-boot-2020.01.txt
         :language: text

Linux Kernel (OS)
-----------------

.. tabs::

   .. tab:: 6.1.74

      .. literalinclude :: logs/bootlog-nand-kernel-6.1.74.txt
         :language: text

   .. tab:: 5.15.105

      .. literalinclude :: logs/bootlog-nand-kernel-5.15.105.txt
         :language: text

   .. tab:: 5.15.105 (RT)

      .. literalinclude :: logs/bootlog-nand-kernel-5.15.105-rt.txt
         :language: text

