.. SPDX-License-Identifier: MIT

SAMA5D2 Xplained Factory Installation to eMMC Logs
==================================================

Here is the complete debug output for booting and running
from the SD card the image for initial factory system
installation to the internal eMMC flash memory.

.. tabs::

   .. tab:: AT91Bootstrap 4.0.8, U-Boot 2023.07, Kernel 6.1.74

      .. literalinclude :: logs/bootlog-factory-installation-a4.0.8-u2023.07-k6.1.74.txt
         :language: text

   .. tab:: AT91Bootstrap 4.0.5, U-Boot 2020.01, Kernel 6.1.74

      .. literalinclude :: logs/bootlog-factory-installation-a4.0.5-u2020.01-k6.1.74.txt
         :language: text

   .. tab:: AT91Bootstrap 3.10.1 (Unsupported), U-Boot 2020.01, Kernel 4.19.78

      .. literalinclude :: logs/bootlog-factory-installation-a3.10.1-u2020.01-k4.19.78.txt
         :language: text
