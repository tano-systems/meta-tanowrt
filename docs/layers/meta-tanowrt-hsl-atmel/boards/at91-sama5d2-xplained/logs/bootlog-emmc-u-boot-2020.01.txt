U-Boot 2020.01-gitAUTOINC+af59b26c22-tano0.2 (Aug 06 2022 - 02:08:09 +0000)

CPU: SAMA5D27-CU
Crystal frequency:       12 MHz
CPU clock        :      498 MHz
Master clock     :      166 MHz
DRAM:  512 MiB
MMC:   sdio-host@a0000000: 0, sdio-host@b0000000: 1
Loading Environment from MMC... OK
In:    serial@f8020000
Out:   serial@f8020000
Err:   serial@f8020000
Net:
Warning: ethernet@f8008000 MAC addresses don't match:
Address in ROM is          00:10:a1:98:97:02
Address in environment is  fc:c2:3d:02:f4:39
eth0: ethernet@f8008000
Hit any key to stop autoboot:  0
switch to partitions #0, OK
mmc0(part 0) is current device

MMC read: dev # 0, block # 512, count 256 ... 256 blocks read: OK
## Executing script at 21000000
Board name: sama5d2_xplained
Active system A
Loading kernel fitImage...
switch to partitions #0, OK
mmc0(part 0) is current device
4614332 bytes read in 274 ms (16.1 MiB/s)
Booting kernel 0x24000000#conf-at91-sama5d2_xplained.dtb...
## Loading kernel from FIT Image at 24000000 ...
   Using 'conf-at91-sama5d2_xplained.dtb' configuration
   Trying 'kernel-1' kernel subimage
     Description:  Linux kernel
     Type:         Kernel Image
     Compression:  uncompressed
     Data Start:   0x24000118
     Data Size:    4567560 Bytes = 4.4 MiB
     Architecture: ARM
     OS:           Linux
     Load Address: 0x22000000
     Entry Point:  0x22000000
     Hash algo:    sha256
     Hash value:   250ea354a19a928c5d40456071b49ca3c972373a34f04987ba277c8c63a09ba1
   Verifying Hash Integrity ... sha256+ OK
## Loading fdt from FIT Image at 24000000 ...
   Using 'conf-at91-sama5d2_xplained.dtb' configuration
   Trying 'fdt-at91-sama5d2_xplained.dtb' fdt subimage
     Description:  Flattened Device Tree blob
     Type:         Flat Device Tree
     Compression:  uncompressed
     Data Start:   0x2445b438
     Data Size:    44779 Bytes = 43.7 KiB
     Architecture: ARM
     Load Address: 0x23e00000
     Hash algo:    sha256
     Hash value:   7afaaebdd0b2fe3b98ef4b716d5b279dfcfd9a928c4bc0874bd95c67fd05ce0c
   Verifying Hash Integrity ... sha256+ OK
   Loading fdt from 0x2445b438 to 0x23e00000
   Booting using the fdt blob at 0x23e00000
   Loading Kernel Image
   Loading Device Tree to 3fb6e000, end 3fb7beea ... OK

Starting kernel ...
