.. SPDX-License-Identifier: MIT

.. _meta-tanowrt-hsl-atmel:

=================================================================
Microchip (Atmel) Hardware Support Layer (meta-tanowrt-hsl-atmel)
=================================================================

meta-tanowrt-hsl-atmel |mdash| `TanoWrt <&TANOWRT_HOMEPAGE;>`_ hardware support
layer (:term:`HSL`) for the Microchip (Atmel) SoC based boards.

.. container:: flex

   .. figure:: /common/images/logos/microchip.svg
      :width: 350

   .. figure:: /common/images/logos/atmel.svg
      :width: 150

.. rubric:: Contents
.. contents::
   :depth: 2
   :local:

Dependencies
============

Dependencies of the :ref:`meta-tanowrt-hsl-atmel <meta-tanowrt-hsl-atmel>` layer are described in the table below.

.. list-table::
   :header-rows: 1
   :width: 100%
   :widths: 15, 40, 15, 15, 15

   * - Layer
     - URI
     - Subfolder
     - Branch
     - Configuration YML-file
   * - :ref:`meta-tanowrt <meta-tanowrt>`
     - &TANOWRT_GIT_URL;
     - meta-tanowrt
     - kirkstone
     - :download:`kas/layers/meta-tanowrt.yml <../../../kas/layers/meta-tanowrt.yml>`
   * - meta-atmel
     - https://github.com/linux4sam/meta-atmel.git
     - --
     - kirkstone
     - :download:`kas/layers/meta-atmel.yml <../../../kas/layers/meta-atmel.yml>`

.. _sec-hsl-atmel-supported-boards:

Supported Boards
================

TanoWrt has demonstration support for some devices and development boards based on Microchip (Atmel) SoC's.
All supported devices are listed in the table below.

.. include:: .layer-boards.rst.inc


.. _sec-hsl-atmel-components-versions:

Components Versions
===================

This layer provides components with the versions listed in the table below.

.. list-table::
   :header-rows: 1
   :width: 100%
   :widths: 20, 20, 20, 40

   * - Component
     - Version(s)
     - Supported Boards
     - Description

   * - AT91Bootstrap
     - | **4.0.8 [Default]**
       | 4.0.6
       | 4.0.5
     - *All*
     - A second stage bootloader for Microchip MPU's

   * - U-Boot
     - | **2023.07 [Default]**
       | 2020.01
     - *All*
     - A third stage bootloader

   * - Kernel
     - | **6.1.74 (non-RT) [Default]**
       | 5.15.105 (non-RT)
       | 5.15.105 (RT)
       | 4.19.78 (non-RT)
     - *All*
     - Linux kernel


License
============

All metadata is MIT licensed unless otherwise stated.
Source code included in tree for individual recipes is under the
LICENSE stated in each recipe (:file:`.bb` file) unless otherwise stated.

Maintainers
============

Anton Kikin <a.kikin@tano-systems.com>

References
==========

1. `Microchip Technology <https://www.microchip.com/>`__
