.. SPDX-License-Identifier: MIT

.. _machine-nanopi-r5c:

***********************
FriendlyElec NanoPi R5C
***********************

.. rubric:: Contents
.. contents::
   :depth: 1
   :local:


.. _sec-nanopi-r5c-board:

Board Overview
==============

The NanoPi R5C is an open-source mini IoT gateway device with two
2.5 Gbit, designed and developed by FriendlyElec. It is integrated with
Rockchip RK3568B2 CPU, 1/2/4 GB LPDDR4X RAM and 8 or 32 GB eMMC flash.
The NanoPi R5C has rich hardware resources with a compact size of 58 |times| 58 mm.

.. _fig-nanopi-r5c:
.. figure:: images/nanopi-r5c.webp
   :width: 900
   :class: with-border

   FriendlyElec NanoPi R5C

Board Layout
------------

.. _fig-nanopi-r5c-layout:
.. figure:: images/nanopi-r5c-layout.jpg
   :width: 750
   :class: with-border

   FriendlyElec NanoPi R5C Layout


Photos
------

.. container:: flex

   .. _fig-nanopi-r5c-overview:
   .. figure:: images/nanopi-r5c-overview.jpg
      :width: 400
      :class: with-border

      FriendlyElec NanoPi R5C Board Overview

   .. _fig-nanopi-r5c-front:
   .. figure:: images/nanopi-r5c-front.jpg
      :width: 400
      :class: with-border

      FriendlyElec NanoPi R5C Board Front View

   .. _fig-nanopi-r5c-back:
   .. figure:: images/nanopi-r5c-back.jpg
      :width: 400
      :class: with-border

      FriendlyElec NanoPi R5C Board Back View

   .. _fig-nanopi-r5c-case-1:
   .. figure:: images/nanopi-r5c-case-1.jpg
      :width: 400
      :class: with-border

      FriendlyElec NanoPi R5C Case Overview

   .. _fig-nanopi-r5c-case-2:
   .. figure:: images/nanopi-r5c-case-2.jpg
      :width: 400
      :class: with-border

      FriendlyElec NanoPi R5C Case Front View

   .. _fig-nanopi-r5c-case-3:
   .. figure:: images/nanopi-r5c-case-3.jpg
      :width: 400
      :class: with-border

      FriendlyElec NanoPi R5C Case Back View

   .. _fig-nanopi-r5c-case-4:
   .. figure:: images/nanopi-r5c-case-4.jpg
      :width: 400
      :class: with-border

      FriendlyElec NanoPi R5C Opened Case


Specification
-------------

.. table:: FriendlyElec NanoPi R5C Specification

   +--------------+------------------------------------------------------------------------------+
   | Model        | FriendlyElec NanoPi R5C                                                      |
   +==============+==============================================================================+
   | Processor    || SoC Rockchip RK3568                                                         |
   |              || Quad Cortex-A55 ARM 64 bits processor                                       |
   |              || frequency up to 2.0 GHz                                                     |
   +--------------+------------------------------------------------------------------------------+
   | GPU          | RM Mali-G52 GPU with support for                                             |
   |              | OpenGL ES 1.1/2.0/3.2, OpenCL 2.0, Vulkan 1.1                                |
   +--------------+------------------------------------------------------------------------------+
   | NPU          | 0.8 TOPS                                                                     |
   +--------------+------------------------------------------------------------------------------+
   | Memory       | 4 GiB LPDDR4X                                                                |
   +--------------+------------------------------------------------------------------------------+
   | Storage      || microSD (support UHS-I)                                                     |
   |              || 8 GiB eMMC flash (up to 128 GiB)                                            |
   +--------------+------------------------------------------------------------------------------+
   | Ethernet     || 2 |times| 2.5 Gigabit Ethernet RJ45 ports                                   |
   |              || (|RTL8125|)                                                                 |
   +--------------+------------------------------------------------------------------------------+
   | PCI Express  || 1 |times| M.2 Key E 2230: PCIe 2.1 x1, USB 2.0 x1 (supports M.2 WiFi,       |
   |              |  SSD and Bluetooth modules)                                                  |
   +--------------+------------------------------------------------------------------------------+
   | USB (Host)   | 2 |times| USB 3.2 Gen 1 Type-A ports                                         |
   +--------------+------------------------------------------------------------------------------+
   | Serial       | 1 |times| Debug UART (3 pin 2.54 mm header, 3.3V level, 1500000 bps)         |
   +--------------+------------------------------------------------------------------------------+
   | HDMI         || Support HDMI 1.4 and HDMI 2.0 operation                                     |
   |              || Support up to 10 bits deep color modes                                      |
   |              || Support up to 1080p\@120 Hz and 4096x2304\@60 Hz                            |
   |              || Support 3D video formats                                                    |
   +--------------+------------------------------------------------------------------------------+
   | LED's        | 4 |times| GPIO controlled LED's (SYS, WAN, LAN, WL)                          |
   +--------------+------------------------------------------------------------------------------+
   | Power        | USB-C 5V input                                                               |
   +--------------+------------------------------------------------------------------------------+
   | Working      | 0°C to 70°C                                                                  |
   | temperature  |                                                                              |
   +--------------+------------------------------------------------------------------------------+
   | Dimensions   || PCB: 8 layer, 58 |times| 58 |times| 1.6 mm)                                 |
   |              ||                                                                             |
   |              || |nanopi-r5c-dimensions|                                                     |
   +--------------+------------------------------------------------------------------------------+

.. |RTL8125| replace:: `Realtek RTL8125 controller <https://www.realtek.com/Product/Index?id=3962&cate_id=786>`__

.. |nanopi-r5c-dimensions| image:: images/nanopi-r5c-case-dimensions.jpg
   :class: with-border
   :width: 418px


.. _sec-nanopi-r5c-targets:

Build Targets
=============

.. _sec-nanopi-r5c-machines:

Machines
--------

.. _table-nanopi-r5c-machines:
.. table:: Supported Machines

   +-----------------+------------------------------+--------------------------+------------------------------------+------------------------+--------------------------+
   | Board\ [#]_     | Target YAML\ [#]_            | Machine\ [#]_            | Target Recipe(s)\ [#]_             | Running Media\ [#]_    | Installation Media\ [#]_ |
   +=================+==============================+==========================+====================================+========================+==========================+
   | NanoPi R5C      | ``nanopi-r5c-sd.yml``        | ``nanopi-r5c-sd``        | ``twimg-default-swu``              | microSD card           | |ndash|                  |
   |                 +------------------------------+--------------------------+------------------------------------+------------------------+--------------------------+
   |                 | ``nanopi-r5c-emmc.yml``      | ``nanopi-r5c-emmc``      | ``twimg-default-swu-factory``      | internal eMMC          | microSD card             |
   +-----------------+------------------------------+--------------------------+------------------------------------+------------------------+--------------------------+

.. [#] Target board.
.. [#] Target YAML-file located in the :file:`kas/targets` directory.
.. [#] Target machine name stored in the ``MACHINE`` BitBake variable for selected Target YAML.
.. [#] Recipes that will be built by default for the target. In :numref:`sec-nanopi-r5c-images`,
       you can find list of supported recipes for the target images, which you can build in addition
       to the default recipes using optional ``--target`` option in build command
       (see :numref:`sec-nanopi-r5c-build`).
.. [#] External or internal data storage where the TanoWrt operating system is running.
.. [#] External storage device for which an installation image is generated. When booting from
       the Installation Media, the TanoWrt system is installed on the Running Media storage.


.. _sec-nanopi-r5c-images:

Images
------

.. _table-nanopi-r5c-images:
.. table:: Supported Images
   :widths: 15, 15, 15, 55

   +---------------------------+------------------------------------+------------------------------+-----------------------------------------------------+
   | Read-Only Root Filesystem | Recipe\ [#]_                       | Supported by Target(s)       | Description                                         |
   | Image                     |                                    |                              |                                                     |
   +===========================+====================================+==============================+=====================================================+
   | ``twimg-default``         | ``twimg-default``                  | *All*                        | Standard TanoWrt image.                             |
   |                           +------------------------------------+------------------------------+-----------------------------------------------------+
   |                           | ``twimg-default-swu``              | *All*                        | Standard TanoWrt image                              |
   |                           |                                    |                              | and :ref:`firmware upgrade <sec-firmware-upgrade>`  |
   |                           |                                    |                              | image. When building this image,                    |
   |                           |                                    |                              | ``twimg-default`` will also be built                |
   |                           |                                    |                              | as dependency.                                      |
   |                           +------------------------------------+------------------------------+-----------------------------------------------------+
   |                           | ``twimg-default-swu-factory``      | ``nanopi-r5c-emmc.yml``      | Factory installation image for standard TanoWrt     |
   |                           |                                    |                              | image. When building this image,                    |
   |                           |                                    |                              | ``twimg-default``                                   |
   |                           |                                    |                              | and ``twimg-default-swu`` will also be built        |
   |                           |                                    |                              | as dependencies.                                    |
   +---------------------------+------------------------------------+------------------------------+-----------------------------------------------------+
   | ``twimg-qt5``             | ``twimg-qt5``                      | *All*                        | Standard TanoWrt image with Qt5 support.            |
   |                           +------------------------------------+------------------------------+-----------------------------------------------------+
   |                           | ``twimg-qt5-swu``                  | *All*                        | Standard TanoWrt image with Qt5 support             |
   |                           |                                    |                              | and :ref:`firmware upgrade <sec-firmware-upgrade>`  |
   |                           |                                    |                              | image. When building this image,                    |
   |                           |                                    |                              | ``twimg-qt5`` will also be built                    |
   |                           |                                    |                              | as dependency.                                      |
   |                           +------------------------------------+------------------------------+-----------------------------------------------------+
   |                           | ``twimg-qt5-swu-factory``          | ``nanopi-r5c-emmc.yml``      | Factory installation image for standard TanoWrt     |
   |                           |                                    |                              | image with Qt5 support. When building this image,   |
   |                           |                                    |                              | ``twimg-qt5``                                       |
   |                           |                                    |                              | and ``twimg-qt5-swu`` will also be built            |
   |                           |                                    |                              | as dependencies.                                    |
   +---------------------------+------------------------------------+------------------------------+-----------------------------------------------------+

.. [#] Image recipe name. This name can be used as argument
       for ``--target`` build command option (see :numref:`sec-nanopi-r5c-build` section).


.. _sec-nanopi-r5c-build:

Build
=====

Please read the common information on how to perform a TanoWrt
images build and preparing the build environment in section ":ref:`sec-build`".

.. seealso:: 

   - See section :numref:`sec-nanopi-r5c-machines` to select the required target YAML file (``<target-yml>``).
   - See section :numref:`sec-nanopi-r5c-images` to select the required root filesystem image recipe (``<target-recipe>``).
   - See section :numref:`sec-nanopi-r5c-artifacts` for detailed information
     about the produced build artifacts.

Examples
--------

Build Default Images for NanoPi R5C Board
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. rubric:: For microSD Card

.. code-block:: console

   $ kas build targets/kas/nanopi-r5c-sd.yml

Default images will be produced to boot and run from the microSD
card on the FriendlyElec NanoPi R5C target board.

.. rubric:: For Internal eMMC Flash

.. code-block:: console

   $ kas build targets/kas/nanopi-r5c-emmc.yml

An initial factory installation image will be generated,
intended to run from the microSD card. The installer image
will install the default image to the internal eMMC flash
memory and further the NanoPi R5C board will boot
and run from the eMMC flash memory.

Build Image with Qt5 Support for NanoPi R5C Board
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. rubric:: For SD Card

.. code-block:: console

   $ kas build --target twimg-qt5-swu targets/kas/nanopi-r5c-sd.yml

Images with Qt5 support will be produced to boot and run from the SD
card on the NanoPi R5C target board.

.. rubric:: For Internal eMMC Flash

.. code-block:: console

   $ kas build --target twimg-qt5-swu-factory targets/kas/nanopi-r5c-emmc.yml

An initial factory installation image will be generated,
intended to run from the SD card. The installer image
will install the image with Qt5 support to the internal eMMC flash
memory and further the NanoPi R5C board will boot
and run from the eMMC flash memory.


.. _sec-nanopi-r5c-partitioning:

Partitioning Layouts
====================

Images for NanoPi R5C board using default partitions layouts described :ref:`here <sec-hsl-rockchip-partitioning>`.

For example, the partitioning and data layout for the 32 GB eMMC
are shown in the figure below.

.. _fig-nanopi-r5c-layout-emmc:
.. figure:: images/nanopi-r5c-layout-emmc.svg
   :width: 1000

   FriendlyElec NanoPi R5C Partitions Layout for eMMC (32 GB)

The eMMC hardware boot partitions 1 (:file:`/dev/mmcblk0boot0`) and
2 (:file:`/dev/mmcblk0boot1`) are currently not used.


.. _sec-nanopi-r5c-artifacts:

Produced Build Artifacts
========================

All produced build artifacts are stored in the :file:`~/tanowrt/build/tanowrt-glibc/deploy/images/<MACHINE>` directory.
Refer to table :ref:`table-nanopi-r5c-artifacts` for a description of some common (not all) build artifacts.

.. _table-nanopi-r5c-artifacts:
.. table:: Produced Build Artifacts
   :widths: 15, 15, 70

   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | Artifact                                                | Target(s)                     | Description                                                   |
   +=========================================================+===============================+===============================================================+
   | .. centered:: Bootloader                                                                                                                                |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | :file:`idblock.img-<MACHINE>-sdcard`                    | *All*                         | Rockchip IDBLOCK image for booting from SD card.              |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | :file:`idblock.img-<MACHINE>-emmc`                      | ``nanopi-r5c-emmc.yml``       | Rockchip IDBLOCK image for booting from internal eMMC.        |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | .. centered:: Bootloader (U-Boot)                                                                                                                       |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | :file:`startup-<MACHINE>.img`                           | *All*                         | U-Boot startup script.                                        |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | :file:`startup-factory-<MACHINE>.img`                   | ``nanopi-r5c-emmc.yml``       | U-Boot startup script for factory installation image.         |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | :file:`u-boot-initial-env-<MACHINE>-sdcard`             | *All*                         | U-Boot initial environment image for SD card image.           |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | :file:`u-boot-initial-env-<MACHINE>-emmc`               | ``nanopi-r5c-emmc.yml``       | U-Boot initial environment image for internal eMMC flash.     |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | :file:`u-boot-<MACHINE>.bin-sdcard`                     | *All*                         | U-Boot binary image for booting from SD card.                 |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | :file:`u-boot-<MACHINE>.bin-emmc`                       | ``nanopi-r5c-emmc.yml``       | U-Boot binary image for booting from internal eMMC flash.     |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | .. centered:: Linux Kernel and DTB                                                                                                                      |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | :file:`fitImage-<MACHINE>.bin`                          | *All*                         | Flattened Image Tree (FIT) image with Linux kernel            |
   |                                                         |                               | and Device Tree Blobs (DTB).                                  |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | :file:`fitImage-<MACHINE>.ext4`                         | *All*                         | FIT image packed into an ext4 file system image.              |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   || :file:`rk3568-nanopi5-rev01-<MACHINE>.dtb`             | *All*                         | Target Device Tree Blobs (DTB).                               |
   || :file:`rk3568-nanopi5-rev02-<MACHINE>.dtb`             |                               |                                                               |
   || :file:`rk3568-nanopi5-rev03-<MACHINE>.dtb`             |                               |                                                               |
   || :file:`rk3568-nanopi5-rev04-<MACHINE>.dtb`             |                               |                                                               |
   || :file:`rk3568-nanopi5-rev05-<MACHINE>.dtb`             |                               |                                                               |
   || :file:`rk3568-nanopi5-rev07-<MACHINE>.dtb`             |                               |                                                               |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | .. centered:: Images                                                                                                                                    |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | :file:`<rootfs-image>-<MACHINE>.sdcard.img`             | ``nanopi-r5c-sd.yml``         | SD card image including all required partitions for booting   |
   |                                                         |                               | and running the system. This image is ready to be written     |
   |                                                         |                               | to the SD card using the :command:`dd` utility or similar     |
   |                                                         |                               | (see :ref:`sec-nanopi-r5c-flash`).                            |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | :file:`<rootfs-image>-swu-factory-<MACHINE>.sdcard.img` | ``nanopi-r5c-emmc.yml``       | SD card factory installation image. This image is ready       |
   |                                                         |                               | to be written to the SD card using the :command:`dd` utility  |
   |                                                         |                               | or similar (see :ref:`sec-nanopi-r5c-flash`).                 |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | :file:`<rootfs-image>-<MACHINE>.squashfs-lzo`           | *All*                         | Root filesystem image (squashfs with LZO compression).        |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+
   | :file:`<rootfs-image>-swu-<MACHINE>.swu`                | *All*                         | :ref:`Firmware upgrade <sec-firmware-upgrade>` image.         |
   +---------------------------------------------------------+-------------------------------+---------------------------------------------------------------+

.. note:: ``<MACHINE>`` in the artifacts path and artifact file names are replaced by
          the actual value of the ``MACHINE`` BitBake variable for the chosen
          `target <sec-nanopi-r5c-targets_>`__. ``<rootfs-image>`` is replaced
          by the actual read-only root filesystem `image <sec-nanopi-r5c-images_>`__ name.

For example, below is the complete lists of artifacts produced by the ``nanopi-r5c-sd.yml``
and ``nanopi-r5c-sd.yml`` target builds. There are two types of listings here |mdash|
a complete listing, and a reduced listing without the symbolic links display.

Build Artifacts Listings for nanopi-r5c-sd.yml Target
-----------------------------------------------------

.. tabs::

   .. tab:: Reduced

      .. code-block:: console

         [~/tanowrt/build/tanowrt-glibc/deploy/images/nanopi-r5c-sd]$ ls -gGh | grep -v -e "^l"
         total 291M
         -rw-r--r-- 2  12M May 14 08:35 fitImage--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.bin
         -rw-r--r-- 2  15M May 14 08:35 fitImage-6.1.57+gitAUTOINC+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd.ext4
         -rw-r--r-- 2 5.8K May 14 08:35 fitImage-its--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.its
         -rw-r--r-- 2  11M May 14 08:35 fitImage-linux.bin--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.bin
         -rw-r--r-- 2  15M May 14 08:35 fitImage-nanopi-r5c-sd.ext4
         -rwxr-xr-x 2 256K May 14 08:35 idblock.img-nanopi-r5c-sd-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         -rwxr-xr-x 2 415K May 14 08:35 loader.bin-nanopi-r5c-sd-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         -rw-r--r-- 2 2.6M May 14 08:35 modules--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.tgz
         -rw-r--r-- 2 171K May 14 08:35 rk3568-nanopi5-rev01--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         -rw-r--r-- 2 170K May 14 08:35 rk3568-nanopi5-rev02--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         -rw-r--r-- 2 170K May 14 08:35 rk3568-nanopi5-rev03--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         -rw-r--r-- 2 170K May 14 08:35 rk3568-nanopi5-rev04--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         -rw-r--r-- 2 171K May 14 08:35 rk3568-nanopi5-rev05--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         -rw-r--r-- 2 172K May 14 08:35 rk3568-nanopi5-rev07--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         -rw-r--r-- 2 2.3K May 14 08:35 startup.img
         -rw-r--r-- 2   16 May 14 08:35 startup.img.version
         -rw-r--r-- 2  70K May 14 08:35 twimg-default-nanopi-r5c-sd-20240514020944.rootfs.manifest
         -rw-r--r-- 2 945M May 14 08:37 twimg-default-nanopi-r5c-sd-20240514020944.rootfs.sdcard.img
         -rw-r--r-- 2  48M May 14 08:36 twimg-default-nanopi-r5c-sd-20240514020944.rootfs.squashfs-lzo
         -rw-r--r-- 2   24 May 14 08:37 twimg-default-nanopi-r5c-sd-20240514020944.rootfs.version
         -rw-r--r-- 2 354K May 14 08:35 twimg-default-nanopi-r5c-sd-20240514020944.testdata.json
         -rw-r--r-- 2 2.2K May 14 08:36 twimg-default-sdimage-rockchip-swu-a-b.wks
         -rw-r--r-- 2  65M May 14 08:38 twimg-default-swu-nanopi-r5c-sd-20240514020944.swu
         -rw-r--r-- 2 5.7K May 14 08:36 twimg-default.env
         -rw-r--r-- 2  405 May 14 08:35 u-boot-initial-env-nanopi-r5c-sd-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13
         -rw-r--r-- 2  32K May 14 08:35 u-boot-initial-env-nanopi-r5c-sd-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         -rw-r--r-- 2 2.0M May 14 08:35 u-boot-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin


   .. tab:: Complete

      .. code-block:: console

         [~/tanowrt/build/tanowrt-glibc/deploy/images/nanopi-r5c-sd]$ ls -gGh
         total 291M
         lrwxrwxrwx 2   82 May 14 08:35 fitImage -> fitImage--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.bin
         -rw-r--r-- 2  12M May 14 08:35 fitImage--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.bin
         -rw-r--r-- 2  15M May 14 08:35 fitImage-6.1.57+gitAUTOINC+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd.ext4
         -rw-r--r-- 2 5.8K May 14 08:35 fitImage-its--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.its
         lrwxrwxrwx 2   86 May 14 08:35 fitImage-its-nanopi-r5c-sd -> fitImage-its--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.its
         -rw-r--r-- 2  11M May 14 08:35 fitImage-linux.bin--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.bin
         lrwxrwxrwx 2   92 May 14 08:35 fitImage-linux.bin-nanopi-r5c-sd -> fitImage-linux.bin--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.bin
         lrwxrwxrwx 2   82 May 14 08:35 fitImage-nanopi-r5c-sd.bin -> fitImage--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.bin
         -rw-r--r-- 2  15M May 14 08:35 fitImage-nanopi-r5c-sd.ext4
         lrwxrwxrwx 2   73 May 14 08:35 idblock.img-nanopi-r5c-sd-sdcard -> idblock.img-nanopi-r5c-sd-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         -rwxr-xr-x 2 256K May 14 08:35 idblock.img-nanopi-r5c-sd-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         lrwxrwxrwx 2   72 May 14 08:35 loader.bin-nanopi-r5c-sd-sdcard -> loader.bin-nanopi-r5c-sd-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         -rwxr-xr-x 2 415K May 14 08:35 loader.bin-nanopi-r5c-sd-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         -rw-r--r-- 2 2.6M May 14 08:35 modules--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.tgz
         lrwxrwxrwx 2   81 May 14 08:35 modules-nanopi-r5c-sd.tgz -> modules--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.tgz
         -rw-r--r-- 2 171K May 14 08:35 rk3568-nanopi5-rev01--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         lrwxrwxrwx 2   94 May 14 08:35 rk3568-nanopi5-rev01-nanopi-r5c-sd.dtb -> rk3568-nanopi5-rev01--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         lrwxrwxrwx 2   94 May 14 08:35 rk3568-nanopi5-rev01.dtb -> rk3568-nanopi5-rev01--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         -rw-r--r-- 2 170K May 14 08:35 rk3568-nanopi5-rev02--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         lrwxrwxrwx 2   94 May 14 08:35 rk3568-nanopi5-rev02-nanopi-r5c-sd.dtb -> rk3568-nanopi5-rev02--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         lrwxrwxrwx 2   94 May 14 08:35 rk3568-nanopi5-rev02.dtb -> rk3568-nanopi5-rev02--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         -rw-r--r-- 2 170K May 14 08:35 rk3568-nanopi5-rev03--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         lrwxrwxrwx 2   94 May 14 08:35 rk3568-nanopi5-rev03-nanopi-r5c-sd.dtb -> rk3568-nanopi5-rev03--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         lrwxrwxrwx 2   94 May 14 08:35 rk3568-nanopi5-rev03.dtb -> rk3568-nanopi5-rev03--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         -rw-r--r-- 2 170K May 14 08:35 rk3568-nanopi5-rev04--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         lrwxrwxrwx 2   94 May 14 08:35 rk3568-nanopi5-rev04-nanopi-r5c-sd.dtb -> rk3568-nanopi5-rev04--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         lrwxrwxrwx 2   94 May 14 08:35 rk3568-nanopi5-rev04.dtb -> rk3568-nanopi5-rev04--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         -rw-r--r-- 2 171K May 14 08:35 rk3568-nanopi5-rev05--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         lrwxrwxrwx 2   94 May 14 08:35 rk3568-nanopi5-rev05-nanopi-r5c-sd.dtb -> rk3568-nanopi5-rev05--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         lrwxrwxrwx 2   94 May 14 08:35 rk3568-nanopi5-rev05.dtb -> rk3568-nanopi5-rev05--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         -rw-r--r-- 2 172K May 14 08:35 rk3568-nanopi5-rev07--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         lrwxrwxrwx 2   94 May 14 08:35 rk3568-nanopi5-rev07-nanopi-r5c-sd.dtb -> rk3568-nanopi5-rev07--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         lrwxrwxrwx 2   94 May 14 08:35 rk3568-nanopi5-rev07.dtb -> rk3568-nanopi5-rev07--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-sd-20240514020944.dtb
         lrwxrwxrwx 2   11 May 14 08:35 startup-nanopi-r5c-sd.img -> startup.img
         lrwxrwxrwx 2   19 May 14 08:35 startup-nanopi-r5c-sd.img.version -> startup.img.version
         -rw-r--r-- 2 2.3K May 14 08:35 startup.img
         -rw-r--r-- 2   16 May 14 08:35 startup.img.version
         -rw-r--r-- 2  70K May 14 08:35 twimg-default-nanopi-r5c-sd-20240514020944.rootfs.manifest
         -rw-r--r-- 2 945M May 14 08:37 twimg-default-nanopi-r5c-sd-20240514020944.rootfs.sdcard.img
         -rw-r--r-- 2  48M May 14 08:36 twimg-default-nanopi-r5c-sd-20240514020944.rootfs.squashfs-lzo
         -rw-r--r-- 2   24 May 14 08:37 twimg-default-nanopi-r5c-sd-20240514020944.rootfs.version
         -rw-r--r-- 2 354K May 14 08:35 twimg-default-nanopi-r5c-sd-20240514020944.testdata.json
         lrwxrwxrwx 2   58 May 14 08:35 twimg-default-nanopi-r5c-sd.manifest -> twimg-default-nanopi-r5c-sd-20240514020944.rootfs.manifest
         lrwxrwxrwx 2   60 May 14 08:37 twimg-default-nanopi-r5c-sd.sdcard.img -> twimg-default-nanopi-r5c-sd-20240514020944.rootfs.sdcard.img
         lrwxrwxrwx 2   62 May 14 08:36 twimg-default-nanopi-r5c-sd.squashfs-lzo -> twimg-default-nanopi-r5c-sd-20240514020944.rootfs.squashfs-lzo
         lrwxrwxrwx 2   56 May 14 08:35 twimg-default-nanopi-r5c-sd.testdata.json -> twimg-default-nanopi-r5c-sd-20240514020944.testdata.json
         lrwxrwxrwx 2   57 May 14 08:37 twimg-default-nanopi-r5c-sd.version -> twimg-default-nanopi-r5c-sd-20240514020944.rootfs.version
         -rw-r--r-- 2 2.2K May 14 08:36 twimg-default-sdimage-rockchip-swu-a-b.wks
         -rw-r--r-- 2  65M May 14 08:38 twimg-default-swu-nanopi-r5c-sd-20240514020944.swu
         lrwxrwxrwx 2   50 May 14 08:38 twimg-default-swu-nanopi-r5c-sd.swu -> twimg-default-swu-nanopi-r5c-sd-20240514020944.swu
         -rw-r--r-- 2 5.7K May 14 08:36 twimg-default.env
         lrwxrwxrwx 2   87 May 14 08:35 u-boot-initial-env-nanopi-r5c-sd-sdcard -> u-boot-initial-env-nanopi-r5c-sd-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13
         -rw-r--r-- 2  405 May 14 08:35 u-boot-initial-env-nanopi-r5c-sd-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13
         -rw-r--r-- 2  32K May 14 08:35 u-boot-initial-env-nanopi-r5c-sd-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         lrwxrwxrwx 2   91 May 14 08:35 u-boot-initial-env-nanopi-r5c-sd-sdcard.bin -> u-boot-initial-env-nanopi-r5c-sd-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         lrwxrwxrwx 2   87 May 14 08:35 u-boot-initial-env-sdcard -> u-boot-initial-env-nanopi-r5c-sd-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13
         lrwxrwxrwx 2   91 May 14 08:35 u-boot-initial-env-sdcard.bin -> u-boot-initial-env-nanopi-r5c-sd-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         lrwxrwxrwx 2   65 May 14 08:35 u-boot-nanopi-r5c-sd.bin -> u-boot-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         lrwxrwxrwx 2   65 May 14 08:35 u-boot-nanopi-r5c-sd.bin-sdcard -> u-boot-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         -rw-r--r-- 2 2.0M May 14 08:35 u-boot-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin


Build Artifacts Listings for nanopi-r5c-emmc.yml Target
-------------------------------------------------------

.. tabs::

   .. tab:: Reduced

      .. code-block:: console

         [~/tanowrt/build/tanowrt-glibc/deploy/images/nanopi-r5c-emmc]$ ls -gGh | grep -v -e "^l"
         total 330M
         -rw-r--r-- 2  12M May 16 03:11 fitImage--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.bin
         -rw-r--r-- 2  15M May 16 03:11 fitImage-6.1.57+gitAUTOINC+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc.ext4
         -rw-r--r-- 2 5.8K May 16 03:11 fitImage-its--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.its
         -rw-r--r-- 2 6.6K May 16 03:11 fitImage-its-twimg-initramfs-swu-factory-nanopi-r5c-emmc--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.its
         -rw-r--r-- 2  11M May 16 03:11 fitImage-linux.bin--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.bin
         -rw-r--r-- 2  15M May 16 03:11 fitImage-nanopi-r5c-emmc.ext4
         -rw-r--r-- 2  27M May 16 03:11 fitImage-twimg-initramfs-swu-factory-6.1.57+gitAUTOINC+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc.ext4
         -rw-r--r-- 2  21M May 16 03:11 fitImage-twimg-initramfs-swu-factory-nanopi-r5c-emmc--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.bin
         -rw-r--r-- 2  27M May 16 03:11 fitImage-twimg-initramfs-swu-factory-nanopi-r5c-emmc.ext4
         -rwxr-xr-x 2 256K May 15 02:50 idblock.img-nanopi-r5c-emmc-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         -rwxr-xr-x 2 256K May 15 02:50 idblock.img-nanopi-r5c-emmc-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         -rwxr-xr-x 2 415K May 15 02:50 loader.bin-nanopi-r5c-emmc-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         -rwxr-xr-x 2 415K May 15 02:50 loader.bin-nanopi-r5c-emmc-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         -rw-r--r-- 2 2.6M May 16 03:11 modules--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.tgz
         -rw-r--r-- 2 171K May 16 03:11 rk3568-nanopi5-rev01--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         -rw-r--r-- 2 170K May 16 03:11 rk3568-nanopi5-rev02--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         -rw-r--r-- 2 170K May 16 03:11 rk3568-nanopi5-rev03--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         -rw-r--r-- 2 170K May 16 03:11 rk3568-nanopi5-rev04--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         -rw-r--r-- 2 171K May 16 03:11 rk3568-nanopi5-rev05--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         -rw-r--r-- 2 172K May 16 03:11 rk3568-nanopi5-rev07--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         -rw-r--r-- 2 1.1K May 15 02:49 startup-factory.img
         -rw-r--r-- 2   16 May 15 02:49 startup-factory.img.version
         -rw-r--r-- 2 2.3K May 15 02:49 startup.img
         -rw-r--r-- 2   16 May 15 02:49 startup.img.version
         -rw-r--r-- 2  71K May 16 03:12 twimg-default-nanopi-r5c-emmc-20240516000713.rootfs.manifest
         -rw-r--r-- 2  47M May 16 03:12 twimg-default-nanopi-r5c-emmc-20240516000713.rootfs.squashfs-lzo
         -rw-r--r-- 2   24 May 16 03:12 twimg-default-nanopi-r5c-emmc-20240516000713.rootfs.version
         -rw-r--r-- 2 355K May 16 03:12 twimg-default-nanopi-r5c-emmc-20240516000713.testdata.json
         -rw-r--r-- 2 101M May 16 03:12 twimg-default-swu-factory-nanopi-r5c-emmc-20240516000713.sdcard.img
         -rw-r--r-- 2 1.4K May 16 03:11 twimg-default-swu-factory-sdimage-rockchip-swu-factory.wks
         -rw-r--r-- 2 6.3K May 16 03:11 twimg-default-swu-factory.env
         -rw-r--r-- 2  64M May 16 03:12 twimg-default-swu-nanopi-r5c-emmc-20240516000713.swu
         -rw-r--r-- 2 9.6M May 16 03:11 twimg-initramfs-swu-factory-nanopi-r5c-emmc-20240516000713.rootfs.cpio.gz
         -rw-r--r-- 2 3.1K May 16 03:10 twimg-initramfs-swu-factory-nanopi-r5c-emmc-20240516000713.rootfs.manifest
         -rw-r--r-- 2   24 May 16 03:11 twimg-initramfs-swu-factory-nanopi-r5c-emmc-20240516000713.rootfs.version
         -rw-r--r-- 2 361K May 16 03:10 twimg-initramfs-swu-factory-nanopi-r5c-emmc-20240516000713.testdata.json
         -rw-r--r-- 2 2.0M May 15 02:50 u-boot-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         -rw-r--r-- 2  405 May 15 02:50 u-boot-initial-env-nanopi-r5c-emmc-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13
         -rw-r--r-- 2  32K May 15 02:50 u-boot-initial-env-nanopi-r5c-emmc-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         -rw-r--r-- 2  405 May 15 02:50 u-boot-initial-env-nanopi-r5c-emmc-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13
         -rw-r--r-- 2  32K May 15 02:50 u-boot-initial-env-nanopi-r5c-emmc-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         -rw-r--r-- 2 2.0M May 15 02:50 u-boot-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin


   .. tab:: Complete

      .. code-block:: console

         [~/tanowrt/build/tanowrt-glibc/deploy/images/nanopi-r5c-emmc]$ ls -gGh
         total 330M
         drwxr-xr-x 2  12K May 16 03:12 .
         drwxr-xr-x 5 4.0K May 15 12:51 ..
         lrwxrwxrwx 2   84 May 16 03:11 fitImage -> fitImage--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.bin
         -rw-r--r-- 2  12M May 16 03:11 fitImage--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.bin
         -rw-r--r-- 2  15M May 16 03:11 fitImage-6.1.57+gitAUTOINC+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc.ext4
         -rw-r--r-- 2 5.8K May 16 03:11 fitImage-its--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.its
         lrwxrwxrwx 2   88 May 16 03:11 fitImage-its-nanopi-r5c-emmc -> fitImage-its--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.its
         -rw-r--r-- 2 6.6K May 16 03:11 fitImage-its-twimg-initramfs-swu-factory-nanopi-r5c-emmc--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.its
         lrwxrwxrwx 2  132 May 16 03:11 fitImage-its-twimg-initramfs-swu-factory-nanopi-r5c-emmc-nanopi-r5c-emmc -> fitImage-its-twimg-initramfs-swu-factory-nanopi-r5c-emmc--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.its
         -rw-r--r-- 2  11M May 16 03:11 fitImage-linux.bin--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.bin
         lrwxrwxrwx 2   94 May 16 03:11 fitImage-linux.bin-nanopi-r5c-emmc -> fitImage-linux.bin--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.bin
         lrwxrwxrwx 2   84 May 16 03:11 fitImage-nanopi-r5c-emmc.bin -> fitImage--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.bin
         -rw-r--r-- 2  15M May 16 03:11 fitImage-nanopi-r5c-emmc.ext4
         -rw-r--r-- 2  27M May 16 03:11 fitImage-twimg-initramfs-swu-factory-6.1.57+gitAUTOINC+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc.ext4
         -rw-r--r-- 2  21M May 16 03:11 fitImage-twimg-initramfs-swu-factory-nanopi-r5c-emmc--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.bin
         lrwxrwxrwx 2  128 May 16 03:11 fitImage-twimg-initramfs-swu-factory-nanopi-r5c-emmc-nanopi-r5c-emmc -> fitImage-twimg-initramfs-swu-factory-nanopi-r5c-emmc--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.bin
         -rw-r--r-- 2  27M May 16 03:11 fitImage-twimg-initramfs-swu-factory-nanopi-r5c-emmc.ext4
         lrwxrwxrwx 2   73 May 15 02:50 idblock.img-nanopi-r5c-emmc-emmc -> idblock.img-nanopi-r5c-emmc-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         -rwxr-xr-x 2 256K May 15 02:50 idblock.img-nanopi-r5c-emmc-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         lrwxrwxrwx 2   75 May 15 02:50 idblock.img-nanopi-r5c-emmc-sdcard -> idblock.img-nanopi-r5c-emmc-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         -rwxr-xr-x 2 256K May 15 02:50 idblock.img-nanopi-r5c-emmc-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         lrwxrwxrwx 2   72 May 15 02:50 loader.bin-nanopi-r5c-emmc-emmc -> loader.bin-nanopi-r5c-emmc-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         -rwxr-xr-x 2 415K May 15 02:50 loader.bin-nanopi-r5c-emmc-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         lrwxrwxrwx 2   74 May 15 02:50 loader.bin-nanopi-r5c-emmc-sdcard -> loader.bin-nanopi-r5c-emmc-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         -rwxr-xr-x 2 415K May 15 02:50 loader.bin-nanopi-r5c-emmc-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55
         -rw-r--r-- 2 2.6M May 16 03:11 modules--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.tgz
         lrwxrwxrwx 2   83 May 16 03:11 modules-nanopi-r5c-emmc.tgz -> modules--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.tgz
         -rw-r--r-- 2 171K May 16 03:11 rk3568-nanopi5-rev01--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         lrwxrwxrwx 2   96 May 16 03:11 rk3568-nanopi5-rev01-nanopi-r5c-emmc.dtb -> rk3568-nanopi5-rev01--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         lrwxrwxrwx 2   96 May 16 03:11 rk3568-nanopi5-rev01.dtb -> rk3568-nanopi5-rev01--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         -rw-r--r-- 2 170K May 16 03:11 rk3568-nanopi5-rev02--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         lrwxrwxrwx 2   96 May 16 03:11 rk3568-nanopi5-rev02-nanopi-r5c-emmc.dtb -> rk3568-nanopi5-rev02--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         lrwxrwxrwx 2   96 May 16 03:11 rk3568-nanopi5-rev02.dtb -> rk3568-nanopi5-rev02--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         -rw-r--r-- 2 170K May 16 03:11 rk3568-nanopi5-rev03--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         lrwxrwxrwx 2   96 May 16 03:11 rk3568-nanopi5-rev03-nanopi-r5c-emmc.dtb -> rk3568-nanopi5-rev03--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         lrwxrwxrwx 2   96 May 16 03:11 rk3568-nanopi5-rev03.dtb -> rk3568-nanopi5-rev03--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         -rw-r--r-- 2 170K May 16 03:11 rk3568-nanopi5-rev04--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         lrwxrwxrwx 2   96 May 16 03:11 rk3568-nanopi5-rev04-nanopi-r5c-emmc.dtb -> rk3568-nanopi5-rev04--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         lrwxrwxrwx 2   96 May 16 03:11 rk3568-nanopi5-rev04.dtb -> rk3568-nanopi5-rev04--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         -rw-r--r-- 2 171K May 16 03:11 rk3568-nanopi5-rev05--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         lrwxrwxrwx 2   96 May 16 03:11 rk3568-nanopi5-rev05-nanopi-r5c-emmc.dtb -> rk3568-nanopi5-rev05--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         lrwxrwxrwx 2   96 May 16 03:11 rk3568-nanopi5-rev05.dtb -> rk3568-nanopi5-rev05--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         -rw-r--r-- 2 172K May 16 03:11 rk3568-nanopi5-rev07--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         lrwxrwxrwx 2   96 May 16 03:11 rk3568-nanopi5-rev07-nanopi-r5c-emmc.dtb -> rk3568-nanopi5-rev07--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         lrwxrwxrwx 2   96 May 16 03:11 rk3568-nanopi5-rev07.dtb -> rk3568-nanopi5-rev07--6.1.57+git0+ea9e2a9344-tano0.2.1.20.1.0-nanopi-r5c-emmc-20240516000713.dtb
         lrwxrwxrwx 2   19 May 15 02:49 startup-factory-nanopi-r5c-emmc.img -> startup-factory.img
         lrwxrwxrwx 2   27 May 15 02:49 startup-factory-nanopi-r5c-emmc.img.version -> startup-factory.img.version
         -rw-r--r-- 2 1.1K May 15 02:49 startup-factory.img
         -rw-r--r-- 2   16 May 15 02:49 startup-factory.img.version
         lrwxrwxrwx 2   11 May 15 02:49 startup-nanopi-r5c-emmc.img -> startup.img
         lrwxrwxrwx 2   19 May 15 02:49 startup-nanopi-r5c-emmc.img.version -> startup.img.version
         -rw-r--r-- 2 2.3K May 15 02:49 startup.img
         -rw-r--r-- 2   16 May 15 02:49 startup.img.version
         -rw-r--r-- 2  71K May 16 03:12 twimg-default-nanopi-r5c-emmc-20240516000713.rootfs.manifest
         -rw-r--r-- 2  47M May 16 03:12 twimg-default-nanopi-r5c-emmc-20240516000713.rootfs.squashfs-lzo
         -rw-r--r-- 2   24 May 16 03:12 twimg-default-nanopi-r5c-emmc-20240516000713.rootfs.version
         -rw-r--r-- 2 355K May 16 03:12 twimg-default-nanopi-r5c-emmc-20240516000713.testdata.json
         lrwxrwxrwx 2   60 May 16 03:12 twimg-default-nanopi-r5c-emmc.manifest -> twimg-default-nanopi-r5c-emmc-20240516000713.rootfs.manifest
         lrwxrwxrwx 2   64 May 16 03:12 twimg-default-nanopi-r5c-emmc.squashfs-lzo -> twimg-default-nanopi-r5c-emmc-20240516000713.rootfs.squashfs-lzo
         lrwxrwxrwx 2   58 May 16 03:12 twimg-default-nanopi-r5c-emmc.testdata.json -> twimg-default-nanopi-r5c-emmc-20240516000713.testdata.json
         lrwxrwxrwx 2   59 May 16 03:12 twimg-default-nanopi-r5c-emmc.version -> twimg-default-nanopi-r5c-emmc-20240516000713.rootfs.version
         -rw-r--r-- 2 101M May 16 03:12 twimg-default-swu-factory-nanopi-r5c-emmc-20240516000713.sdcard.img
         lrwxrwxrwx 2   67 May 16 03:12 twimg-default-swu-factory-nanopi-r5c-emmc.sdcard.img -> twimg-default-swu-factory-nanopi-r5c-emmc-20240516000713.sdcard.img
         -rw-r--r-- 2 1.4K May 16 03:11 twimg-default-swu-factory-sdimage-rockchip-swu-factory.wks
         -rw-r--r-- 2 6.3K May 16 03:11 twimg-default-swu-factory.env
         -rw-r--r-- 2  64M May 16 03:12 twimg-default-swu-nanopi-r5c-emmc-20240516000713.swu
         lrwxrwxrwx 2   52 May 16 03:12 twimg-default-swu-nanopi-r5c-emmc.swu -> twimg-default-swu-nanopi-r5c-emmc-20240516000713.swu
         -rw-r--r-- 2 9.6M May 16 03:11 twimg-initramfs-swu-factory-nanopi-r5c-emmc-20240516000713.rootfs.cpio.gz
         -rw-r--r-- 2 3.1K May 16 03:10 twimg-initramfs-swu-factory-nanopi-r5c-emmc-20240516000713.rootfs.manifest
         -rw-r--r-- 2   24 May 16 03:11 twimg-initramfs-swu-factory-nanopi-r5c-emmc-20240516000713.rootfs.version
         -rw-r--r-- 2 361K May 16 03:10 twimg-initramfs-swu-factory-nanopi-r5c-emmc-20240516000713.testdata.json
         lrwxrwxrwx 2   73 May 16 03:11 twimg-initramfs-swu-factory-nanopi-r5c-emmc.cpio.gz -> twimg-initramfs-swu-factory-nanopi-r5c-emmc-20240516000713.rootfs.cpio.gz
         lrwxrwxrwx 2   74 May 16 03:10 twimg-initramfs-swu-factory-nanopi-r5c-emmc.manifest -> twimg-initramfs-swu-factory-nanopi-r5c-emmc-20240516000713.rootfs.manifest
         lrwxrwxrwx 2   72 May 16 03:10 twimg-initramfs-swu-factory-nanopi-r5c-emmc.testdata.json -> twimg-initramfs-swu-factory-nanopi-r5c-emmc-20240516000713.testdata.json
         lrwxrwxrwx 2   73 May 16 03:11 twimg-initramfs-swu-factory-nanopi-r5c-emmc.version -> twimg-initramfs-swu-factory-nanopi-r5c-emmc-20240516000713.rootfs.version
         -rw-r--r-- 2 2.0M May 15 02:50 u-boot-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         lrwxrwxrwx 2   87 May 15 02:50 u-boot-initial-env-emmc -> u-boot-initial-env-nanopi-r5c-emmc-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13
         lrwxrwxrwx 2   91 May 15 02:50 u-boot-initial-env-emmc.bin -> u-boot-initial-env-nanopi-r5c-emmc-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         lrwxrwxrwx 2   87 May 15 02:50 u-boot-initial-env-nanopi-r5c-emmc-emmc -> u-boot-initial-env-nanopi-r5c-emmc-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13
         -rw-r--r-- 2  405 May 15 02:50 u-boot-initial-env-nanopi-r5c-emmc-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13
         -rw-r--r-- 2  32K May 15 02:50 u-boot-initial-env-nanopi-r5c-emmc-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         lrwxrwxrwx 2   91 May 15 02:50 u-boot-initial-env-nanopi-r5c-emmc-emmc.bin -> u-boot-initial-env-nanopi-r5c-emmc-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         lrwxrwxrwx 2   89 May 15 02:50 u-boot-initial-env-nanopi-r5c-emmc-sdcard -> u-boot-initial-env-nanopi-r5c-emmc-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13
         -rw-r--r-- 2  405 May 15 02:50 u-boot-initial-env-nanopi-r5c-emmc-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13
         -rw-r--r-- 2  32K May 15 02:50 u-boot-initial-env-nanopi-r5c-emmc-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         lrwxrwxrwx 2   93 May 15 02:50 u-boot-initial-env-nanopi-r5c-emmc-sdcard.bin -> u-boot-initial-env-nanopi-r5c-emmc-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         lrwxrwxrwx 2   89 May 15 02:50 u-boot-initial-env-sdcard -> u-boot-initial-env-nanopi-r5c-emmc-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13
         lrwxrwxrwx 2   93 May 15 02:50 u-boot-initial-env-sdcard.bin -> u-boot-initial-env-nanopi-r5c-emmc-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         lrwxrwxrwx 2   63 May 15 02:50 u-boot-nanopi-r5c-emmc.bin -> u-boot-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         lrwxrwxrwx 2   63 May 15 02:50 u-boot-nanopi-r5c-emmc.bin-emmc -> u-boot-emmc-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         lrwxrwxrwx 2   65 May 15 02:50 u-boot-nanopi-r5c-emmc.bin-sdcard -> u-boot-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin
         -rw-r--r-- 2 2.0M May 15 02:50 u-boot-sdcard-2017.09+gitAUTOINC+4695fbcfba_642a85dd55-tano13.bin


.. _sec-nanopi-r5c-flash:

Writing Images
==============


.. _sec-nanopi-r5c-flash-sd:

Writing Image to microSD Card
-----------------------------

No special information about writing images to microSD card
for NanoPi R5C board. See common instructions in :ref:`sec-writing-sd-or-usb` section.

.. rubric:: Examples

Writing factory installation image for the ``nanopi-r5c-emmc.yml`` target to the microSD
card :file:`/dev/mmcblk1`:

.. code-block:: console

   $ dd if=twimg-default-swu-factory-nanopi-r5c-emmc.sdcard.img of=/dev/mmcblk1

Writing bootable image for the ``nanopi-r5c-sd.yml`` target to the microSD
card :file:`/dev/mmcblk1`:

.. code-block:: console

   $ dd if=twimg-default-nanopi-r5c-sd.sdcard.img of=/dev/mmcblk1


.. _sec-nanopi-r5c-flash-emmc:

Writing Image to eMMC Flash
---------------------------

For the initial flashing of the internal eMMC memory it is recommended to use
the special image of the initial factory installation. If you choose a build target
(see :ref:`sec-nanopi-r5c-targets` for details) that assumes using the
factory installation image for the initial flashing of the
device, a factory installation image (:file:`<rootfs-image>-swu-factory-<MACHINE>.sdcard.img`)
will be automatically generated during the build process
(see :ref:`sec-nanopi-r5c-build`).
To write the factory installation image to a SD card, follow the instructions
from :ref:`sec-writing-sd-or-usb` section.

When you boot device from the prepaired SD card with factory installation image the installation
of TanoWrt to the internal eMMC flash memory will be done automatically. The detailed
installation log is available on the :ref:`debug UART <sec-nanopi-r5c-serial>`. After the installation is complete,
the board will reboots automatically. When the device is rebooted, the installed system
will be booted from the internal eMMC flash memory.

.. caution:: Be aware that during the installation all existing data on the internal eMMC
             flash memory will be permanently lost.


.. _sec-nanopi-r5c-booting:

Booting and Running
===================

Booting from SD Card
--------------------

1. Insert the micro SD card to the board.
2. Connect the USB-C power cable to the input connector.
3. NanoPi R5C will boot.
4. (Optional) Use a USB to TTL serial cable to make a connection between
   your PC and NanoPi R5C. See :ref:`sec-nanopi-r5c-serial` section for details.
5. Log in to system using default :ref:`credentials <sec-access-creds>`.

.. attention:: The internal eMMC flash memory must be empty in order to perform
               a boot from the SD card. See :ref:`sec-nanopi-r5c-emmc-erase`
               section for details about erasing internal eMMC flash.


Booting from Internal eMMC Flash
-----------------------------------

1. Connect the USB-C power cable to the input connector.
2. NanoPi R5C will boot.
3. (Optional) Use a USB to TTL serial cable to make a connection between
   your PC and NanoPi R5C. See :ref:`sec-nanopi-r5c-serial` section for details.
4. Log in to system using default :ref:`credentials <sec-access-creds>`.


.. _sec-nanopi-r5c-emmc-erase:

Erasing Internal eMMC Flash
------------------------------

You can erase the internal eMMC flash memory in the following ways:

- Linux command line;
- U-Boot command line;
- special utilities and USB connection with PC.

.. tabs::

   .. tab:: Linux CLI

      .. rubric:: Erasing eMMC Using Linux Command Line

      If the device has a bootable Linux system, you can erase the eMMC
      by the following command entered in the Linux terminal:

      .. code-block:: console

         [root@tanowrt ~]# dd if=/dev/zero of=/dev/mmcblk0 bs=1k count=32768

      Note that if the Linux distribution flashed on the eMMC flash memory
      is not TanoWrt, the device name of the eMMC flash memory may be different.
      You can usually find out the real device name by analysing the
      system boot log by running the command dmesg. For example:

      .. code-block:: console
         :emphasize-lines: 2,3,4,5,6

         [root@tanowrt ~]# dmesg | grep mmcblk
         [    1.450311] mmcblk0: mmc2:0001 A3A551 28.9 GiB
         [    1.452242] mmcblk0boot0: mmc2:0001 A3A551 4.00 MiB
         [    1.453387] mmcblk0boot1: mmc2:0001 A3A551 4.00 MiB
         [    1.454417] mmcblk0rpmb: mmc2:0001 A3A551 16.0 MiB, chardev (237:0)
         [    1.574956] mmcblk1: mmc0:59b4 SMI   7.50 GiB
         [    1.585584]  mmcblk1: p1 p2 p3 p4 p5 p6
         [    9.188921] overlay-resize: Root device mmcblk1
         [    9.393006] overlay-resize: Overlay partition /dev/mmcblk1p6 (disk /dev/mmcblk1)
         [   11.313657] overlay-resize: Partition /dev/mmcblk1p6 end sector 1933311 -> 15728606
         [   11.313939] overlay-resize: Resizing filesystem on partition /dev/mmcblk1p6...
         [   24.781017] mount_root: /dev/mmcblk1p1: p1, rw, start 8388608, size 8388608
         [   24.786788] mount_root: /dev/mmcblk1p2: p2, rw, start 16777216, size 67108864
         [   24.787141] mount_root: /dev/mmcblk1p3: p3, rw, start 83886080, size 402653184 [rootfs]
         [   24.793086] mount_root: /dev/mmcblk1p4: p4, rw, start 486539264, size 67108864
         [   24.794989] mount_root: /dev/mmcblk1p5: p5, rw, start 553648128, size 402653184
         [   24.800779] mount_root: /dev/mmcblk1p6: p6, rw, start 956301312, size 7096745472 [overlay]
         [   24.800966] mount_root: root filesystem on the /dev/mmcblk1p3 partition of /dev/mmcblk1 (rw) device
         [   24.821481] mount_root: founded suitable overlay partition /dev/mmcblk1p6
         [   25.215651] EXT4-fs (mmcblk1p6): mounted filesystem with ordered data mode. Quota mode: disabled.
         [   26.700054] EXT4-fs (mmcblk1p6): unmounting filesystem.
         [   26.786359] EXT4-fs (mmcblk1p6): mounted filesystem with ordered data mode. Quota mode: disabled.

      This output shows that device :file:`/dev/mmcblk0` is eMMC flash
      memory and device :file:`/dev/mmcblk1` is SD card.

   .. tab:: U-Boot CLI

      .. rubric:: Erasing eMMC Using U-Boot Command Line

      If the device has a functional U-Boot bootloader, you can erase
      eMMC with the following commands entered at the U-Boot command line:

      .. code-block:: console

         => mmc dev 0
         switch to partitions #0, OK
         mmc0 is current device
         => mmc erase 0 10000

         MMC erase: dev # 0, block # 0, count 65536 ... 65536 blocks erased: OK
         =>

      Please note that if the U-Boot bootloader on the eMMC flash memory
      is different from the one built as part of the TanoWrt distribution,
      the device number of the eMMC flash memory may be different from
      the one shown above. Use the :command:`mmc dev`, :command:`mmc list` and :command:`mmc info`
      commands to identify the valid eMMC flash memory device number:

      .. code-block:: console
         :emphasize-lines: 3,5,8

         => mmc dev 0
         switch to partitions #0, OK
         mmc0(part 0) is current device
         => mmc info
         Device: sdhci@fe310000
         Manufacturer ID: d6
         OEM: 103
         Name: A3A55
         Timing Interface: HS200
         Tran Speed: 200000000
         Rd Block Len: 512
         MMC version 5.1
         High Capacity: Yes
         Capacity: 28.9 GiB
         Bus Width: 8-bit
         Erase Group Size: 512 KiB
         HC WP Group Size: 8 MiB
         User Capacity: 28.9 GiB WRREL
         Boot Capacity: 4 MiB ENH
         RPMB Capacity: 16 MiB ENH
         =>

   .. tab:: USB Connection with PC

      .. rubric:: Erasing eMMC Using Special Utilities and USB Connection with PC

      If the device fails to boot, e.g. due to a corrupted boot loader,
      the only way to erase the eMMC flash memory is to use a USB
      connection with a PC and use special utilities.

      .. todo:: Add content


.. _sec-nanopi-r5c-serial:

Serial Console
==============

.. note:: The default baudrate of NanoPi R5C with original firmware is 1500000 (1.5 Mbps),
          please check if your USB to TTL cable support 1.5 Mbps baudrate. Some model
          of CP210X and PL2303x have baudrate limitation, please check the specified model.

.. rubric:: Connection

Connect the USB to TTL serial cable as described below. Don't connect the VCC wire,
connect only TX, RX and GND wires.

+--------+----------------------------+
| Signal | NanoPi R5C Board           |
|        +----------------------------+
|        | 3-pin connector J9510      |
|        | (header next to LAN/USB-C) |
+========+============================+
| GND    | Pin 1 (GND)                |
+--------+----------------------------+
| TX     | Pin 2 (UART2DBG_TX)        |
+--------+----------------------------+
| RX     | Pin 3 (UART2DBG_RX)        |
+--------+----------------------------+

See :ref:`fig-nanopi-r5c-usb-to-ttl` figure for example
connection USB to TTL converter with NanoPi R5C.

.. _fig-nanopi-r5c-usb-to-ttl:
.. figure:: images/nanopi-r5c-usb-to-ttl.jpg
    :class: with-border
    :width: 800

    USB to TTL Converter Connection to NanoPi R5C


.. rubric:: Serial Setting on Host PC

The default serial console settings for NanoPi R5C for U-Boot and kernel are
described in table below.

+-----------------+-------------------+
| Parameter       | Value             |
+=================+===================+
| Baudrate        | 1500000           |
+-----------------+-------------------+
| Data bits       | 8                 |
+-----------------+-------------------+
| Stop bits       | 1                 |
+-----------------+-------------------+
| Parity          | none              |
+-----------------+-------------------+
| Flow control    | none              |
+-----------------+-------------------+
| Signal level    | 3.3 V             |
+-----------------+-------------------+


.. _sec-nanopi-r5c-network-config:

Default Network Configuration
=============================

By default, network port LAN (interface ``eth1``) are joined
into a bridge (``br-lan`` interface).
Bridge ``br-lan`` is in the LAN firewall zone. By default, the IP address
on the ``br-lan`` bridge is configured using a :term:`DHCP` client.

To see obtained IP configuration for LAN network use the following command:

.. code-block:: console

   [root@tanowrt ~]# ifstatus lan | jsonfilter -e '@["ipv4-address"][0].address'
   192.168.0.54

In this example, the device got the IP address 192.168.0.54 via :term:`DHCP`
on LAN interface (bridge ``br-lan``).

The network port WAN (interface ``eth0``) is a separate network interface
included in the WAN firewall zone with enabled translation (NAT) from LAN zone.
The IP address of the ``eth0`` interface is also configured with a :term:`DHCP` client.
A firewall with blocking rules for incoming traffic is enabled on the ``eth0``
interface. Therefore, there is no access to the web configuration interface
through this interface.

To see obtained IP configuration for WAN network use the following command:

.. code-block:: console

   [root@tanowrt ~]# ifstatus wan | jsonfilter -e '@["ipv4-address"][0].address'
   10.10.0.168

In this example, the device got the IP address 10.10.0.168 via :term:`DHCP`
on the WAN interface (``eth0``).

Ethernet ports LAN (``eth1``) and WAN (``eth0``)
have enabled :term:`LLDP` by default.


.. _sec-nanopi-r5c-webui:

Web User Interface
==================

The WebUI can be accessed via Ethernet port connection through HTTP(s) protocol.
You must see something like this in browser after you logged in:

.. _fig-nanopi-r5c-luci-login:
.. figure:: /common/images/luci/page-login.png
   :width: 900

   LuCI WebUI Login Page

.. _fig-nanopi-r5c-luci-status:
.. figure:: images/nanopi-r5c-luci-status.png
   :width: 900

   LuCI WebUI Overview Page


.. _sec-nanopi-r5c-upgrade:

Firmware Upgrade
================

No special information about firmware upgrade.
Use produced :file:`.swu` :ref:`artifact <table-nanopi-r5c-artifacts>` for upgrading running system.

.. seealso:: See common instructions in :ref:`sec-firmware-upgrade` section.


Additional Information
======================

Here are sections with various additional information about the
FriendlyElec NanoPi R5C board and the operation of TanoWrt on it.

.. toctree::

   bootlog-sd.rst
   bootlog-emmc.rst
   bootlog-factory-installation.rst


References
==========

1. https://www.friendlyelec.com/index.php?route=product/product&product_id=290
2. https://wiki.friendlyelec.com/wiki/index.php/NanoPi_R5C#Hardware_Spec
