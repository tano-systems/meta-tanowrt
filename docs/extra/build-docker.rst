
==================================
Using Dockerized Build Environment
==================================

Clone TanoWrt repository to your home directory (this document uses ``~/tanowrt`` for example):

.. code-block:: console

   user@host:/ $ cd ~
   user@host:~ $ git clone --recursive &TANOWRT_GIT_URL; tanowrt
   user@host:~ $ cd ~/tanowrt

Build docker image:

.. code-block:: console

   user@host:~/tanowrt $ cd .ci
   user@host:~/tanowrt/.ci $ docker build -t ubuntu:tanowrt-build \
                                          --build-arg host_uid=$(id -u) \
                                          --build-arg host_gid=$(id -g) \
                                          -f Dockerfile .

.. tip:: By default, a user ``build`` is created in the built docker image. You can change the
         user name by adding the following parameter to the docker build command:

         .. code-block::

            --build-arg username=<username>

Run created docker image and mount cloned TanoWrt repository to it:

.. code-block:: console

   user@host:~/tanowrt $ docker run --rm -it -v ~/tanowrt:/tanowrt ubuntu:tanowrt-build /bin/bash
   build@abbc5ae583ff:~$ 

Now TanoWrt repository at :file:`~/tanowrt` on host is mounted to the :file:`/tanowrt`
directory in docker. Now you can use kas utility to building TanoWrt and all other
available build commands described in the :ref:`sec-build` section.

For example, build TanoWrt default images for the ``qemux86-64-screen`` machine:

.. code-block:: console

   build@abbc5ae583ff:~$ cd /tanowrt
   build@abbc5ae583ff:/tanowrt$ kas build kas/targets/qemux86-64-screen.yml

The built images are available both in the
:file:`/tanowrt/build/tanowrt-glibc/deploy/images/qemux86-64-screen`
directory in the docker and in
:file:`~/tanowrt/build/tanowrt-glibc/deploy/images/qemux86-64-screen`
in the host system.


.. _sec-build-docker-dl-dir:

Specifying the Downloads Directory
----------------------------------

If you have a separate directory for storing downloads by the build process
you can use it in the docker by adding following parameter to :command:`docker run` command:

.. code-block::

   -v <path-to-downloads-on-host>:/tanowrt/build/downloads

For example, if you have a :file:`~/oe-downloads` directory for storing downloads
on the host, your :command:`docker run` command should be like this:

.. code-block:: console
   :emphasize-lines: 2

   user@host:~/tanowrt $ docker run --rm -it -v ~/tanowrt:/tanowrt \
                                             -v ~/oe-downloads:/tanowrt/build/downloads \
                                              ubuntu:tanowrt-build /bin/bash

.. important:: The downloads directory (:file:`~/oe-downloads`) must be exists
               and accessible for user running docker container.

.. note:: The downloads directory can be mounted in another location than
          :file:`/tanowrt/build/downloads`, but in this case, you must explicitly
          specify the path to this directory in the
          :ref:`local configuration <sec-build-local-conf>`
          in the ``DL_DIR`` variable.

          .. code-block:: console

             build@abbc5ae583ff:~$ echo 'DL_DIR = "<path-to-downloads-dir>"' >> /tanowrt/local.conf


.. _sec-build-docker-sstate-cache-dir:

Specifying the Shared State Cache Directory
-------------------------------------------

If you have a separate directory for storing shared state cache by the build process
you can use it in the docker by adding following parameter to :command:`docker run` command:

.. code-block::

   -v <path-to-sstate-cache>:/tanowrt/build/sstate-cache

For example, if you have a :file:`~/oe-sstate-cache` directory for storing shared
state cache on the host, your :command:`docker run` command should be like this:

.. code-block:: console
   :emphasize-lines: 2

   user@host:~/tanowrt $ docker run --rm -it -v ~/tanowrt:/tanowrt \
                                             -v ~/oe-sstate-cache:/tanowrt/build/sstate-cache \
                                              ubuntu:tanowrt-build /bin/bash

.. important:: The shared state cache directory (:file:`~/oe-sstate-cache`)
               must be exists and accessible for user running docker container.

.. note:: The shared state cache directory can be mounted in another location than
          :file:`/tanowrt/build/sstate-cache`, but in this case, you must explicitly
          specify the path to this directory in the
          :ref:`local configuration <sec-build-local-conf>`
          in the ``SSTATE_DIR`` variable:

          .. code-block:: console

             build@abbc5ae583ff:~$ echo 'SSTATE_DIR = "<path-to-sstate-cache-dir>"' >> /tanowrt/local.conf


Complete Example
----------------

Finally, here is the complete command to run the docker container for the TanoWrt build:

.. code-block:: console

   user@host:~/tanowrt $ docker run --rm -it -v ~/tanowrt:/tanowrt \
                                             -v ~/oe-downloads:/tanowrt/build/downloads \
                                             -v ~/oe-sstate-cache:/tanowrt/build/sstate-cache \
                                              ubuntu:tanowrt-build /bin/bash

Using this command the local :ref:`sstate-cache directory <sec-build-docker-dl-dir>`
and the local :ref:`directory for storing downloaded data <sec-build-docker-dl-dir>`
will be mounted to the docker container.

.. important:: The shared state cache (:file:`~/oe-sstate-cache`) and downloads (:file:`~/oe-downloads`)
               directories must be exists and accessible for user running docker container.
