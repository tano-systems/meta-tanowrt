# meta-tanowrt-hsl-baikal

[TanoWrt](https://gitlab.com/tano-systems/meta-tanowrt) hardware support layer for the Baikal Electronics SoC boards.

![Baikal Electronics](../docs/common/images/logos/baikal-electronics.png)

## 1 Documentation

You can find documentation for this layer [here](https://tano-systems.gitlab.io/meta-tanowrt/layers/meta-tanowrt-hsl-baikal).

## 2 License

All metadata is MIT licensed unless otherwise stated. Source code included in tree for individual recipes is under the LICENSE stated in each recipe (.bb file) unless otherwise stated.

## 3 Maintainers

Anton Kikin <a.kikin@tano-systems.com>
